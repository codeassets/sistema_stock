﻿Imports System.ComponentModel

Public Class pruebaPRODUCTOS
    Dim columnxy As String
    Dim SetSortOrder As ListSortDirection
    Dim GridSortOrder As SortOrder
    Dim opcion As Integer
    Private Sub pruebaPRODUCTOS_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CB_Buscar.SelectedIndex = 6
    End Sub



    Private Sub txtBuscar_KeyDown(sender As Object, e As KeyEventArgs) Handles txtBuscar.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim indu As New DatosProductos

            'buscar por el combobox

            Try

                If (txtBuscar.Text <> "") Then
                    Dim dt As New DataTable
                    Dim Bandera As New Integer

                    If CB_Buscar.SelectedIndex = 0 Then 'codigo
                        indu.Codigo_ = txtBuscar.Text
                        Bandera = 0

                    ElseIf CB_Buscar.SelectedIndex = 1 Then 'talle
                        indu.Talle_ = txtBuscar.Text
                        Bandera = 1

                    ElseIf CB_Buscar.SelectedIndex = 2 Then 'marca
                        indu.Marca_ = txtBuscar.Text
                        Bandera = 2
                    ElseIf CB_Buscar.SelectedIndex = 3 Then 'color
                        indu.colorname_ = txtBuscar.Text
                        Bandera = 3
                    ElseIf CB_Buscar.SelectedIndex = 4 Then 'descripcion
                        indu.Descripcion_ = txtBuscar.Text
                        Bandera = 4
                    ElseIf CB_Buscar.SelectedIndex = 5 Then 'desc y marca
                        If (txtBuscar.Text.Contains(" ")) Then
                            indu.Descripcion_ = txtBuscar.Text.Split(" ")(0).Replace(" ", "")
                            indu.Marca_ = txtBuscar.Text.Split(" ")(1).Replace(" ", "")
                        Else
                            indu.Descripcion_ = txtBuscar.Text
                        End If

                        Bandera = 5


                    End If

                    If CB_Buscar.SelectedIndex = 6 Then 'buscar por todos los campos
                        dgvstock.DataSource = indu.BuscarXtodosloscampos(txtBuscar.Text)
                    Else
                        dgvstock.DataSource = indu.Buscar(dt, Bandera)
                    End If

                Else
                    dgvstock.DataSource = indu.ObtenerTodo()
                End If


                dgvstock.Columns("Color").Visible = False

                If dgvstock.RowCount <> 0 Then
                    For Each row As DataGridViewRow In dgvstock.Rows
                        row.Cells("NombreColor").Style.BackColor = Color.FromArgb(Convert.ToInt32(row.Cells("Color").Value))
                    Next
                End If
                dgvstock.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dgvstock.Columns("Marca").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dgvstock.Columns("Cantidad").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
                dgvstock.Columns("Talle").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
                dgvstock.Columns("NombreColor").HeaderText = "Color"
                dgvstock.Columns("PrecioMinorista").HeaderText = "$ Min."
                dgvstock.Columns("PrecioMayorista").HeaderText = "$ May."
                dgvstock.Columns("PrecioTarjeta").HeaderText = "$ Tarj."
                dgvstock.Columns("Cantidad").HeaderText = "Cant."
                dgvstock.ClearSelection()
            Catch ex As Exception
                MessageBox.Show(ex.Message.ToString)
            End Try
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim p As New DatosProductos
        dgvstock.DataSource = p.VerificarCodRepetidos()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim p As New DatosProductos
        dgvstock.DataSource = p.VerificarProductosRepetidos()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If tbUsuario.Text = "codeassets" And TBContraseña.Text = "codeassets" Then
            PanelLogin.Visible = False
            PanelPrincipal.Visible = True
            Dim indu As New DatosProductos
            Dim dt As New DataTable
            dgvstock.DataSource = indu.ObtenerTodo()
            If (columnxy <> Nothing) Then
                dgvstock.Sort(dgvstock.Columns(columnxy), SetSortOrder)
            End If

            If dgvstock.RowCount <> 0 Then
                For Each row As DataGridViewRow In dgvstock.Rows
                    row.Cells("NombreColor").Style.BackColor = Color.FromArgb(Convert.ToInt32(row.Cells("Color").Value))
                Next
            End If

            dgvstock.Columns("Color").Visible = False

            dgvstock.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            dgvstock.Columns("Marca").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            dgvstock.Columns("Cantidad").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
            dgvstock.Columns("Talle").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
            dgvstock.Columns("NombreColor").HeaderText = "Color"
            dgvstock.Columns("PrecioMinorista").HeaderText = "$ Min."
            dgvstock.Columns("PrecioMayorista").HeaderText = "$ May."
            dgvstock.Columns("PrecioTarjeta").HeaderText = "$ Tarj."
            dgvstock.Columns("Cantidad").HeaderText = "Cant."
        Else
            lblError.Visible = True
            lblError.Text = "Datos Incorrectos"
            tbUsuario.Text = ""
            TBContraseña.Text = ""
        End If
    End Sub

    Private Sub bttEditarProd_Click(sender As Object, e As EventArgs) Handles bttEditarProd.Click
        Try
            opcion = 2
            txtCod.Text = dgvstock.SelectedRows(0).Cells("Codigo").Value
            CBoxMarca.Text = dgvstock.SelectedRows(0).Cells("Marca").Value
            txtDesc.Text = dgvstock.SelectedRows(0).Cells("Descripcion").Value
            CBoxTalle.Text = dgvstock.SelectedRows(0).Cells("Talle").Value
            bttColor.BackColor = Color.FromArgb(Convert.ToInt32(dgvstock.SelectedRows(0).Cells("Color").Value))
            tb_colorname.Text = dgvstock.SelectedRows(0).Cells("NombreColor").Value
            txtPrecioMi.Text = dgvstock.SelectedRows(0).Cells("PrecioMinorista").Value
            txtPrecioMa.Text = dgvstock.SelectedRows(0).Cells("PrecioMayorista").Value
            txtPrecioTa.Text = dgvstock.SelectedRows(0).Cells("PrecioTarjeta").Value
            NumCant.Value = dgvstock.SelectedRows(0).Cells("Cantidad").Value
        Catch ex As Exception
            MessageBox.Show("Seleccione el producto que desea modificar. Detalle: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub bttGuardarProd_Click(sender As Object, e As EventArgs) Handles bttGuardarProd.Click
        If opcion = 2 Then 'MODIFICAR


            If CBoxMarca.SelectedIndex <> -1 And txtDesc.Text <> "" And CBoxTalle.SelectedIndex <> -1 And tb_colorname.Text <> "" And txtPrecioMa.Text <> "" And txtPrecioMi.Text <> "" And txtPrecioTa.Text <> "" Then
                Try

                    Dim ind As New DatosProductos
                    ind.Marca_ = CBoxMarca.Text
                    ind.Descripcion_ = txtDesc.Text
                    ind.Talle_ = CBoxTalle.Text
                    ind.Color_ = bttColor.BackColor.ToArgb
                    ind.colorname_ = tb_colorname.Text
                    ind.PrecioMinorista_ = txtPrecioMi.Text
                    ind.PrecioMayorista_ = txtPrecioMa.Text
                    ind.PrecioTarjeta_ = txtPrecioTa.Text
                    ind.Cantidad_ = NumCant.Value
                    If (txtCod.Text = "") Then
                        'Modificar Producto con Codigo Aleatorio
                        ind.Codigo_ = "i_" + (CInt(Math.Ceiling(Rnd() * 999999)) + 1).ToString + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss")
                        ind.ModificarIndumentaria(dgvstock.SelectedRows(0).Cells("IdProducto").Value)
                        MsgBox("Se modificó el producto con un codigo Aleatorio Nuevo.", MsgBoxStyle.Information, Title:="")
                    Else
                        'si el codigo es el mismo que el dgv entonces no se modifico el txtCodigo
                        If txtCod.Text = dgvstock.SelectedRows(0).Cells("Codigo").Value Then
                            'Modificar Producto con el mismo codigo
                            ind.Codigo_ = Trim(txtCod.Text)
                            ind.ModificarIndumentaria(dgvstock.SelectedRows(0).Cells("IdProducto").Value)
                            MsgBox("Se modificó el producto con éxito y no se modifico el Codigo.", MsgBoxStyle.Information, Title:="")
                        Else
                            ind.Codigo_ = txtCod.Text
                            Dim buscar_producto = ind.BuscarxCodigo()
                            If buscar_producto.rows.count() > 0 Then
                                'Error por ingresar un Codigo Existente
                                MsgBox("El código del producto ya esta en uso. Deje el campo en blanco para generar un código interno.", MsgBoxStyle.Information, Title:="")
                                Return
                            Else
                                'Modificar Productos con un nuevo Codigo no Existente
                                ind.Codigo_ = Trim(txtCod.Text)
                                ind.ModificarIndumentaria(dgvstock.SelectedRows(0).Cells("IdProducto").Value)
                                MsgBox("Se modificó el producto con éxito con nuevo codigo.", MsgBoxStyle.Information, Title:="")
                            End If
                        End If

                    End If
                    dgvstock.Enabled = True


                Catch ex As Exception
                    MessageBox.Show("Se produjo un error al modificar el producto. Detalle: " & ex.Message.ToString)
                End Try
            Else
                MessageBox.Show("Complete todos los campos.")
            End If



        End If
    End Sub

    Private Sub bttEliminar_Click(sender As Object, e As EventArgs) Handles bttEliminar.Click

        Dim ind As New DatosProductos
        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea eliminar este producto del inventario? Detalle: ID=" & dgvstock.SelectedRows(0).Cells("IdProducto").Value, "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Try
                ind.IdProducto_ = dgvstock.SelectedRows(0).Cells("IdProducto").Value
                ind.EliminarProductoDELABD()
                MsgBox("Se eliminó el producto con éxito.", MsgBoxStyle.Information, Title:="")
            Catch ex As Exception
                MessageBox.Show("Se produjo un error al eliminar el producto. Detalle: " & ex.Message.ToString)
            End Try
        End If
    End Sub
End Class