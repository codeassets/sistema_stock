﻿Imports System.Data.SqlClient

Public Class DatosProductos

    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

#Region "GetSet"

    Private IdProducto As Integer
    Public Property IdProducto_() As Integer
        Get
            Return IdProducto
        End Get
        Set(ByVal value As Integer)
            IdProducto = value
        End Set
    End Property

    Private Codigo As String
    Public Property Codigo_() As String
        Get
            Return Codigo
        End Get
        Set(ByVal value As String)
            Codigo = value
        End Set
    End Property

    Private Marca As String
    Public Property Marca_() As String
        Get
            Return Marca
        End Get
        Set(ByVal value As String)
            Marca = value
        End Set
    End Property

    Private IdMarca As Integer
    Public Property IdMarca_() As Integer
        Get
            Return IdMarca
        End Get
        Set(ByVal value As Integer)
            IdMarca = value
        End Set
    End Property

    Private Descripcion As String
    Public Property Descripcion_() As String
        Get
            Return Descripcion
        End Get
        Set(ByVal value As String)
            Descripcion = value
        End Set
    End Property

    Private Talle As String
    Public Property Talle_() As String
        Get
            Return Talle
        End Get
        Set(ByVal value As String)
            Talle = value
        End Set
    End Property

    Private Color As String
    Public Property Color_() As String
        Get
            Return Color
        End Get
        Set(ByVal value As String)
            Color = value
        End Set
    End Property
    Private colorname As String
    Public Property colorname_() As String
        Get
            Return colorname
        End Get
        Set(ByVal value As String)
            colorname = value
        End Set
    End Property

    Private PrecioMinorista As Double
    Public Property PrecioMinorista_() As Double
        Get
            Return PrecioMinorista
        End Get
        Set(ByVal value As Double)
            PrecioMinorista = value
        End Set
    End Property

    Private PrecioMayorista As Double
    Public Property PrecioMayorista_() As Double
        Get
            Return PrecioMayorista
        End Get
        Set(ByVal value As Double)
            PrecioMayorista = value
        End Set
    End Property

    Private PrecioTarjeta As Double
    Public Property PrecioTarjeta_() As Double
        Get
            Return PrecioTarjeta
        End Get
        Set(ByVal value As Double)
            PrecioTarjeta = value
        End Set
    End Property

    Private Cantidad As Integer
    Public Property Cantidad_() As Integer
        Get
            Return Cantidad
        End Get
        Set(ByVal value As Integer)
            Cantidad = value
        End Set
    End Property

    Private Fecha_A As DateTime
    Public Property Fecha_A_() As DateTime
        Get
            Return Fecha_A
        End Get
        Set(ByVal value As DateTime)
            Fecha_A = value
        End Set
    End Property

    Private Fecha_M As DateTime
    Public Property Fecha_M_() As DateTime
        Get
            Return Fecha_M
        End Get
        Set(ByVal value As DateTime)
            Fecha_M = value
        End Set
    End Property

    Private IdUsuario_A As Int64
    Public Property IdUsuario_A_() As Int64
        Get
            Return IdUsuario_A
        End Get
        Set(ByVal value As Int64)
            IdUsuario_A = value
        End Set
    End Property

    Private IdUsuario_M As Int64
    Public Property Id_Usuario_M_() As Int64
        Get
            Return IdUsuario_M
        End Get
        Set(ByVal value As Int64)
            IdUsuario_M = value
        End Set
    End Property

#End Region

#Region "Funciones de Verificacion de Integridad"
    Public Function VerificarCodRepetidos() As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim Accion As String

        Dim Tabla As New DataTable

        Accion = "SELECT Codigo, count([Codigo]) as Cantidad FROM [Stock].[dbo].[Productos] group by Codigo order by Cantidad desc"
        Dim Adaptador As New SqlDataAdapter(Accion, Conexion)
        Tabla.Clear()

        Conexion.Open()
        Adaptador.Fill(Tabla)
        Conexion.Close()

        Return Tabla

    End Function
    Public Function VerificarProductosRepetidos() As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim Accion As String

        Dim Tabla As New DataTable

        Accion = "Select Marca,Descripcion,Talle,NombreColor,count(*) as Cantidad from Productos group by Marca,Descripcion,Talle,NombreColor order by Cantidad desc"
        Dim Adaptador As New SqlDataAdapter(Accion, Conexion)
        Tabla.Clear()

        Conexion.Open()
        Adaptador.Fill(Tabla)
        Conexion.Close()

        Return Tabla

    End Function

    Public Function BuscarXtodosloscampos(valor As String) As DataTable
        Dim dt As New DataTable
        Dim comando As New SqlCommand
        comando.Parameters.AddWithValue("@valor", "%" & valor & "%")


        comando.CommandText = "SELECT * from Productos where idproducto like @valor or codigo like @valor or marca like @valor or descripcion like @valor or talle like @valor or nombrecolor like @valor order by Descripcion asc"
        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function
    Public Sub EliminarProductoDELABD()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdProducto", IdProducto)
        Comando.CommandText = "DELETE from Productos WHERE IdProducto = @IdProducto"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub
#End Region

    Public Function ObtenerTodo() As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim Accion As String

        Dim Tabla As New DataTable

        Accion = "SELECT * FROM Productos where Activo = 1 order by Codigo"
        Dim Adaptador As New SqlDataAdapter(Accion, Conexion)
        Tabla.Clear()

        Conexion.Open()
        Adaptador.Fill(Tabla)
        Conexion.Close()

        Return Tabla

    End Function

    Public Function Buscar(ByVal dt As DataTable, ByVal Bandera As Integer) As DataTable


        If Bandera = 0 Then 'cod
            Dim comando As New SqlCommand
            comando.Parameters.AddWithValue("@Codigo", Codigo)
            comando.CommandText = "SELECT * from Productos where Codigo = @Codigo and Activo = 1"
            comando.Connection = New SqlConnection(Cadena)
            Dim adaptador As New SqlDataAdapter(comando)
            comando.Connection.Open()
            adaptador.Fill(dt)
            comando.Connection.Close()

        ElseIf Bandera = 1 Then 'talle
            Using adaptador As New SqlDataAdapter("SELECT * " +
                                              "from Productos " +
                                              "where talle = @talle and Activo = 1 ORDER BY Descripcion", Cadena)
                adaptador.SelectCommand.Parameters.AddWithValue("@talle", Talle)
                adaptador.Fill(dt)
            End Using

        ElseIf Bandera = 2 Then 'marca

            Using adaptador As New SqlDataAdapter("SELECT * " +
                                              "from Productos " +
                                              "where marca  LIKE @marca and Activo = 1 ORDER BY " +
                                              "marca", Cadena)
                adaptador.SelectCommand.Parameters.AddWithValue("@marca", "%" & Marca & "%")
                adaptador.Fill(dt)
            End Using

        ElseIf Bandera = 3 Then 'color

            Using adaptador As New SqlDataAdapter("SELECT * " +
                                              "from Productos " +
                                              "where NombreColor  Like @nomcolor and Activo = 1 ORDER BY NombreColor", Cadena)
                adaptador.SelectCommand.Parameters.AddWithValue("@nomcolor", "%" & colorname & "%")
                adaptador.Fill(dt)
            End Using

        ElseIf Bandera = 4 Then 'descripcion

            Using adaptador As New SqlDataAdapter("SELECT * " +
                                              "from Productos " +
                                              "where descripcion  LIKE @desc and Activo = 1 ORDER BY descripcion", Cadena)
                adaptador.SelectCommand.Parameters.AddWithValue("@desc", "%" & Descripcion & "%")
                adaptador.Fill(dt)
            End Using

        ElseIf Bandera = 5 Then 'desc-marca

            If (Marca Is Nothing) Then
                Using adaptador As New SqlDataAdapter("SELECT * " +
                                                  "from Productos " +
                                                  "where descripcion  Like @desc and Activo = 1 ORDER BY descripcion", Cadena)
                    adaptador.SelectCommand.Parameters.AddWithValue("@desc", "%" & Descripcion & "%")
                    adaptador.Fill(dt)
                End Using

            Else
                Using adaptador As New SqlDataAdapter("SELECT * " +
                                  "from Productos " +
                                  "where marca  LIKE @marca and descripcion LIKE @desc and Activo = 1 ORDER BY descripcion", Cadena)
                    adaptador.SelectCommand.Parameters.AddWithValue("@desc", "%" & Descripcion & "%")
                    adaptador.SelectCommand.Parameters.AddWithValue("@marca", "%" & Marca & "%")
                    adaptador.Fill(dt)
                End Using
            End If

        ElseIf Bandera = 6 Then 'idproducto
            Using adaptador As New SqlDataAdapter("SELECT * " +
                                              "from Productos " +
                                              "where IdProducto = @id and Activo = 1 ORDER BY descripcion", Cadena)
                adaptador.SelectCommand.Parameters.AddWithValue("@id", IdProducto)
                adaptador.Fill(dt)
            End Using

        End If

        Return dt

    End Function

    Public Function ReporteTodosProductos(orden As String, ascdesc As String) As DataTable
        Dim dt As New DataTable
        Dim comando As New SqlCommand


        If ascdesc = "ASC" Then
            comando.CommandText = "SELECT IdProducto as NroProducto, Codigo, Descripcion, Marca, Talle, NombreColor as Color, PrecioMinorista, PrecioMayorista, PrecioTarjeta, Cantidad from Productos where Activo = 1 order by '" & orden & "' asc"
        Else
            comando.CommandText = "SELECT IdProducto as NroProducto, Codigo, Descripcion, Marca, Talle, NombreColor as Color, PrecioMinorista, PrecioMayorista, PrecioTarjeta, Cantidad from Productos where Activo = 1 order by '" & orden & "' desc"
        End If

        comando.Connection = New SqlConnection(Cadena)
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(dt)
        comando.Connection.Close()
        Return dt

    End Function

    Public Function TraerOrdenado(ByVal dt As DataTable, ByVal Bandera As Integer) As DataTable

        If Bandera = 0 Then 'cod

            Dim comando As New SqlCommand
            comando.Parameters.AddWithValue("@Codigo", Codigo)
            comando.CommandText = "SELECT * from Productos where Codigo =@Codigo and Activo = 1"
            comando.Connection = New SqlConnection(Cadena)
            Dim adaptador As New SqlDataAdapter(comando)
            comando.Connection.Open()
            adaptador.Fill(dt)
            comando.Connection.Close()

        ElseIf Bandera = 1 Then 'talle

            Using adaptador As New SqlDataAdapter("SELECT * " +
                                              "from Productos " +
                                              "where talle = @talle and Activo = 1 ORDER BY Descripcion", Cadena)
                adaptador.SelectCommand.Parameters.AddWithValue("@talle", Talle)
                adaptador.Fill(dt)
            End Using

        ElseIf Bandera = 2 Then 'marca

            Using adaptador As New SqlDataAdapter("SELECT * " +
                                              "from Productos " +
                                              "where marca  LIKE @marca and Activo = 1 ORDER BY " +
                                              "marca", Cadena)
                adaptador.SelectCommand.Parameters.AddWithValue("@marca", "%" & Marca & "%")
                adaptador.Fill(dt)
            End Using

        ElseIf Bandera = 3 Then 'color

            Using adaptador As New SqlDataAdapter("SELECT * " +
                                              "from Productos " +
                                              "where NombreColor  Like @nomcolor and Activo = 1 ORDER BY NombreColor", Cadena)
                adaptador.SelectCommand.Parameters.AddWithValue("@nomcolor", "%" & colorname & "%")
                adaptador.Fill(dt)
            End Using

        ElseIf Bandera = 4 Then 'descripcion

            Using adaptador As New SqlDataAdapter("SELECT * " +
                                              "from Productos " +
                                              "where descripcion  LIKE @desc and Activo = 1 ORDER BY descripcion", Cadena)
                adaptador.SelectCommand.Parameters.AddWithValue("@desc", "%" & Descripcion & "%")
                adaptador.Fill(dt)
            End Using

        ElseIf Bandera = 5 Then 'desc-marca

            If (Marca Is Nothing) Then
                Using adaptador As New SqlDataAdapter("SELECT * " +
                                                  "from Productos " +
                                                  "where descripcion  Like @desc and Activo = 1 ORDER BY descripcion", Cadena)
                    adaptador.SelectCommand.Parameters.AddWithValue("@desc", "%" & Descripcion & "%")
                    adaptador.Fill(dt)
                End Using

            Else
                Using adaptador As New SqlDataAdapter("SELECT * " +
                                  "from Productos " +
                                  "where marca  LIKE @marca and descripcion LIKE @desc and Activo = 1 ORDER BY descripcion", Cadena)
                    adaptador.SelectCommand.Parameters.AddWithValue("@desc", "%" & Descripcion & "%")
                    adaptador.SelectCommand.Parameters.AddWithValue("@marca", "%" & Marca & "%")
                    adaptador.Fill(dt)
                End Using
            End If

        End If

        Return dt

    End Function

    Public Function ObtenerFaltantes(ByVal cantidad As Integer) As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim Accion As String

        Dim Tabla As New DataTable

        Accion = "SELECT IdProducto, Codigo, Descripcion, Marca, Talle, NombreColor, Color, PrecioMinorista, PrecioMayorista, PrecioTarjeta, Cantidad FROM Productos where Activo = 1 and Cantidad <= " + cantidad.ToString & " order by Cantidad desc"
        Dim Adaptador As New SqlDataAdapter(Accion, Conexion)
        Tabla.Clear()

        Conexion.Open()
        Adaptador.Fill(Tabla)
        Conexion.Close()

        Return Tabla

    End Function

    Public Function ObtenerPrecioMinorista() As Double

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim precio As Double

        Comando.Parameters.AddWithValue("@Codigo", Codigo)

        Comando.CommandText = "SELECT PrecioMinorista from Productos WHERE @Codigo= Codigo and Activo = 1"
        Comando.Connection = Conexion
        Conexion.Open()
        precio = Comando.ExecuteScalar()
        Conexion.Close()
        Return precio

    End Function

    Public Function ObtenerPrecioTarjeta()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim precio As Double

        Comando.Parameters.AddWithValue("@Codigo", Codigo)

        Comando.CommandText = "SELECT PrecioTarjeta from Productos WHERE @Codigo= Codigo and Activo = 1"
        Comando.Connection = Conexion
        Conexion.Open()
        precio = Comando.ExecuteScalar()
        Conexion.Close()
        Return precio

    End Function

    Public Function ObtenerPrecioMayorista()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim precio As Double

        Comando.Parameters.AddWithValue("@Codigo", Codigo)

        Comando.CommandText = "SELECT PrecioMayorista from Productos WHERE @Codigo= Codigo and Activo = 1"
        Comando.Connection = Conexion
        Conexion.Open()
        precio = Comando.ExecuteScalar()
        Conexion.Close()
        Return precio

    End Function

    Public Function Buscar(ByVal valor As String) As DataTable

        Dim dt As New DataTable

        Using adaptador As New SqlDataAdapter("SELECT * " +
                                              "from Productos " +
                                              "where Codigo LIKE '" & "%" + valor + "%" & "' or Marca  LIKE '" & "%" + valor + "%" & "' or Descripcion  LIKE '" & "%" + valor + "%" & "' or Color  LIKE '" & "%" + valor + "%" & "' or NombreColor  LIKE '" & "%" + valor + "%" & "' and Activo = 1  ORDER BY Marca", Cadena)
            adaptador.Fill(dt)
        End Using

        Return dt

    End Function

    Public Function BuscarxCodigo()

        Dim comando As New SqlCommand
        comando.Parameters.AddWithValue("@Codigo", Codigo)
        comando.CommandText = "SELECT * from Productos where Codigo =@Codigo and Activo = 1"
        comando.Connection = New SqlConnection(Cadena)
        Dim tabla As New DataTable
        Dim adaptador As New SqlDataAdapter(comando)
        comando.Connection.Open()
        adaptador.Fill(tabla)
        comando.Connection.Close()
        Return tabla

    End Function

    Public Sub InsertarIndumentaria()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@Codigo", Codigo)
        Comando.Parameters.AddWithValue("@Marca", Marca)
        Comando.Parameters.AddWithValue("@Descripcion", Descripcion)
        Comando.Parameters.AddWithValue("@Talle", Talle)
        Comando.Parameters.AddWithValue("@Color", Color)
        Comando.Parameters.AddWithValue("@ncolor", colorname)
        Comando.Parameters.AddWithValue("@PrecioMinorista", PrecioMinorista)
        Comando.Parameters.AddWithValue("@PrecioMayorista", PrecioMayorista)
        Comando.Parameters.AddWithValue("@PrecioTarjeta", PrecioTarjeta)
        Comando.Parameters.AddWithValue("@Cantidad", Cantidad)
        'Comando.Parameters.AddWithValue("@Fecha_A", Fecha_A)
        'Comando.Parameters.AddWithValue("@Fecha_M", Fecha_M)
        'Comando.Parameters.AddWithValue("@IdUsuario_A", 1)
        'Comando.Parameters.AddWithValue("@IdUsuario_M", 1)

        Comando.CommandText = "INSERT INTO Productos(Codigo, Marca, Descripcion, Talle, Color,NombreColor,
                                PrecioMinorista, PrecioMayorista, PrecioTarjeta, Cantidad, Fecha_A, Fecha_M, IdUsuario_A, IdUsuario_M) " +
                               "VALUES( @Codigo, @Marca, @Descripcion, @Talle, @Color,@ncolor,
                                @PrecioMinorista, @PrecioMayorista, @PrecioTarjeta, @Cantidad, GETDATE(), GETDATE(), 1, 1)"

        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()
    End Sub

    Public Sub ModificarIndumentaria(id As Integer)

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@Codigo", Codigo)
        Comando.Parameters.AddWithValue("@Marca", Marca)
        Comando.Parameters.AddWithValue("@Descripcion", Descripcion)
        Comando.Parameters.AddWithValue("@Talle", Talle)
        Comando.Parameters.AddWithValue("@Color", Color)
        Comando.Parameters.AddWithValue("@ncolor", colorname)
        Comando.Parameters.AddWithValue("@PrecioMinorista", PrecioMinorista)
        Comando.Parameters.AddWithValue("@PrecioMayorista", PrecioMayorista)
        Comando.Parameters.AddWithValue("@PrecioTarjeta", PrecioTarjeta)
        Comando.Parameters.AddWithValue("@Cantidad", Cantidad)
        Comando.Parameters.AddWithValue("@IdUsuario_M", 1)

        Comando.CommandText = "UPDATE Productos SET Codigo=@Codigo, Marca = @Marca," +
        "Descripcion = @Descripcion, Talle = @Talle, Color = @Color,NombreColor=@ncolor, 
            PrecioMinorista = @PrecioMinorista, PrecioMayorista = @PrecioMayorista, PrecioTarjeta = @PrecioTarjeta, Cantidad = @Cantidad," +
        " Fecha_M = GETDATE(), IdUsuario_M = @IdUsuario_M WHERE IdProducto = '" & id & "'"

        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub EliminarProducto()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdProducto", IdProducto)
        Comando.Parameters.AddWithValue("@IdUsuario_M", IdUsuario_M)

        Comando.CommandText = "UPDATE Productos SET Activo = 0, Fecha_M = GETDATE(), IdUsuario_M = @IdUsuario_M WHERE IdProducto = @IdProducto"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub DisminuirProducto()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdProducto", IdProducto)
        Comando.Parameters.AddWithValue("@Cantidad", Cantidad)

        Comando.CommandText = "UPDATE Productos SET Cantidad = Cantidad - @Cantidad WHERE IdProducto = @IdProducto"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub AumentarProducto()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdProducto", IdProducto)
        Comando.Parameters.AddWithValue("@Cantidad", Cantidad)

        Comando.CommandText = "UPDATE Productos Set Cantidad = Cantidad + @Cantidad WHERE IdProducto = @IdProducto"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub InsertarMarca()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Comando.Parameters.AddWithValue("@Marca", Marca)
        Comando.CommandText = "INSERT INTO Marcas(Nombre) " +
                               "VALUES(@Marca)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ModificarMarca()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@IdMarca", IdMarca)
        Comando.Parameters.AddWithValue("@Nombre", Marca)

        Comando.CommandText = "UPDATE Marcas SET Nombre = @Nombre WHERE IdMarca = @IdMarca"

        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub EliminarMarca()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdMarca", IdMarca)
        Comando.CommandText = "UPDATE Marcas SET Activo = 0 WHERE IdMarca = @IdMarca"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Function ObtenerTodoMarca() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("SELECT * from Marcas where Activo = 1", Cadena)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function

    Public Function VerificarCodBarras() As Boolean

        Dim cn As New SqlConnection
        Dim cmd As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet

        cn.ConnectionString = Cadena
        cmd.CommandType = CommandType.Text
        cmd.Parameters.AddWithValue("@cod", Codigo)
        cmd.CommandText = "SELECT Codigo FROM Productos WHERE Codigo = @cod and Activo = 1"

        cmd.Connection = cn
        cn.Open()
        Dim nro_encontrado As String = cmd.ExecuteScalar

        cn.Close()

        If nro_encontrado = Nothing Then
            Return False
        Else
            Return True 'llenar campos
        End If

    End Function

End Class
