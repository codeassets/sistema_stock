﻿Public Class ConfigVendedores
    Dim opcion As Integer

    Private Sub ConfigVendedores_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim vendedor As New DatosVendedores
        dgvVendedores.DataSource = vendedor.ObtenerTodo
        dgvVendedores.Columns("IdVendedor").Visible = False
        dgvVendedores.Columns("Nombre").HeaderText = "Vendedor"
    End Sub

    Private Sub dgvVendedores_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvVendedores.CellClick
        bttModificar.Enabled = True
        bttEliminar.Enabled = True

        Try
            txtVendedor.Text = dgvVendedores.CurrentRow.Cells("Nombre").Value
        Catch ex As Exception

        End Try
    End Sub

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click
        ' Agregar
        txtVendedor.Enabled = True
        txtVendedor.Clear()
        txtVendedor.Focus()
        bttAgregar.Enabled = False
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvVendedores.Enabled = False

        opcion = 1
    End Sub

    Private Sub bttModificar_Click(sender As Object, e As EventArgs) Handles bttModificar.Click
        ' Modificar
        txtVendedor.Enabled = True
        txtVendedor.Focus()
        bttAgregar.Enabled = False
        bttEliminar.Enabled = False
        bttModificar.Enabled = False
        bttGuardar.Enabled = True
        bttCancelar.Enabled = True
        dgvVendedores.Enabled = False

        opcion = 2
    End Sub

    Private Sub bttEliminar_Click(sender As Object, e As EventArgs) Handles bttEliminar.Click
        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea eliminar este registro?", "Confirmación", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Try
                Dim vendedor As New DatosVendedores
                vendedor.IdVendedor_ = dgvVendedores.SelectedRows(0).Cells("IdVendedor").Value
                vendedor.EliminarVendedor()
                MsgBox("Se eliminó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                dgvVendedores.Enabled = True
                dgvVendedores.DataSource = vendedor.ObtenerTodo()
                bttCancelar.PerformClick()
            Catch ex As Exception
                MessageBox.Show("Ocurrió un problema al eliminar el registro. Detalle:" & ex.Message.ToString)
            End Try
        End If

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click
        dgvVendedores.Enabled = True
        dgvVendedores.ClearSelection()

        bttAgregar.Enabled = True
        bttModificar.Enabled = False
        bttEliminar.Enabled = False
        bttGuardar.Enabled = False

        txtVendedor.Enabled = False
        txtVendedor.Clear()
    End Sub

    Private Sub bttGuardar_Click(sender As Object, e As EventArgs) Handles bttGuardar.Click

        If txtVendedor.Text <> "" Then

            Dim vendedor As New DatosVendedores

            If opcion = 1 Then 'AGREGAR
                Try
                    vendedor.Nombre_ = txtVendedor.Text
                    vendedor.InsertarVendedor()
                    MsgBox("Registro creado con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvVendedores.Enabled = True
                    dgvVendedores.DataSource = vendedor.ObtenerTodo()

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al crear el registro. Detalle:" & ex.Message.ToString)
                End Try

            ElseIf opcion = 2 Then 'MODIFICAR
                Try

                    vendedor.Nombre_ = txtVendedor.Text
                    vendedor.IdVendedor_ = dgvVendedores.SelectedRows(0).Cells("IdVendedor").Value
                    vendedor.ModificarVendedor()
                    MsgBox("Se modificó el registro con éxito.", MsgBoxStyle.Information, Title:="Confirmación")
                    dgvVendedores.Enabled = True
                    dgvVendedores.DataSource = vendedor.ObtenerTodo

                Catch ex As Exception
                    MessageBox.Show("Ocurrió un problema al modificar el registro. Detalle:" & ex.Message.ToString)
                End Try
            End If

            bttCancelar.PerformClick()
        Else
            MessageBox.Show("Complete todos los datos.")
        End If
    End Sub
End Class