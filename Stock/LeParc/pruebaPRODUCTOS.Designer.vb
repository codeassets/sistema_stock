﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pruebaPRODUCTOS
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.dgvstock = New System.Windows.Forms.DataGridView()
        Me.CB_Buscar = New System.Windows.Forms.ComboBox()
        Me.txtBuscar = New MetroFramework.Controls.MetroTextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PanelPrincipal = New System.Windows.Forms.Panel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblError = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.TBContraseña = New System.Windows.Forms.TextBox()
        Me.tbUsuario = New System.Windows.Forms.TextBox()
        Me.PanelLogin = New System.Windows.Forms.Panel()
        Me.bttEliminar = New System.Windows.Forms.Button()
        Me.bttEditarProd = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.txtPrecioMi = New System.Windows.Forms.TextBox()
        Me.txtPrecioMa = New System.Windows.Forms.TextBox()
        Me.bttGuardarProd = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.NumCant = New System.Windows.Forms.NumericUpDown()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tb_colorname = New System.Windows.Forms.TextBox()
        Me.txtPrecioTa = New System.Windows.Forms.TextBox()
        Me.bttColor = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.CBoxTalle = New System.Windows.Forms.ComboBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.CBoxMarca = New System.Windows.Forms.ComboBox()
        Me.txtDesc = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtCod = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        CType(Me.dgvstock, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.PanelPrincipal.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.PanelLogin.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.NumCant, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvstock
        '
        Me.dgvstock.AllowUserToAddRows = False
        Me.dgvstock.AllowUserToDeleteRows = False
        Me.dgvstock.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvstock.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvstock.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvstock.Location = New System.Drawing.Point(42, 154)
        Me.dgvstock.MultiSelect = False
        Me.dgvstock.Name = "dgvstock"
        Me.dgvstock.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvstock.Size = New System.Drawing.Size(815, 493)
        Me.dgvstock.TabIndex = 0
        '
        'CB_Buscar
        '
        Me.CB_Buscar.BackColor = System.Drawing.Color.White
        Me.CB_Buscar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_Buscar.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CB_Buscar.FormattingEnabled = True
        Me.CB_Buscar.Items.AddRange(New Object() {"Código", "Talle", "Marca", "Color", "Descripción", "Descripción y Marca", "Todos los Campos"})
        Me.CB_Buscar.Location = New System.Drawing.Point(250, 115)
        Me.CB_Buscar.Name = "CB_Buscar"
        Me.CB_Buscar.Size = New System.Drawing.Size(216, 28)
        Me.CB_Buscar.TabIndex = 3
        '
        'txtBuscar
        '
        Me.txtBuscar.BackColor = System.Drawing.Color.White
        Me.txtBuscar.Icon = Global.Stock.My.Resources.Resources.buscador_musical__1_
        Me.txtBuscar.Lines = New String(-1) {}
        Me.txtBuscar.Location = New System.Drawing.Point(42, 117)
        Me.txtBuscar.Margin = New System.Windows.Forms.Padding(20, 8, 3, 8)
        Me.txtBuscar.MaxLength = 32767
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtBuscar.PromptText = "Buscar"
        Me.txtBuscar.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtBuscar.SelectedText = ""
        Me.txtBuscar.Size = New System.Drawing.Size(186, 26)
        Me.txtBuscar.TabIndex = 2
        Me.txtBuscar.UseSelectable = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(42, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(424, 91)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Verificaciones de Integridad"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(184, 51)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Comprobar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(186, 19)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Comprobar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(165, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Verificar Indumentarias Repetidas"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(137, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Verificar Codigos Repetidos"
        '
        'PanelPrincipal
        '
        Me.PanelPrincipal.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelPrincipal.Controls.Add(Me.GroupBox4)
        Me.PanelPrincipal.Controls.Add(Me.txtBuscar)
        Me.PanelPrincipal.Controls.Add(Me.CB_Buscar)
        Me.PanelPrincipal.Controls.Add(Me.dgvstock)
        Me.PanelPrincipal.Controls.Add(Me.GroupBox3)
        Me.PanelPrincipal.Controls.Add(Me.GroupBox1)
        Me.PanelPrincipal.Location = New System.Drawing.Point(12, 12)
        Me.PanelPrincipal.Name = "PanelPrincipal"
        Me.PanelPrincipal.Size = New System.Drawing.Size(880, 790)
        Me.PanelPrincipal.TabIndex = 5
        Me.PanelPrincipal.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.bttEliminar)
        Me.GroupBox3.Controls.Add(Me.bttEditarProd)
        Me.GroupBox3.Location = New System.Drawing.Point(472, 13)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(385, 91)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblError)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.btnAceptar)
        Me.GroupBox2.Controls.Add(Me.TBContraseña)
        Me.GroupBox2.Controls.Add(Me.tbUsuario)
        Me.GroupBox2.Location = New System.Drawing.Point(178, 69)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(441, 200)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Login"
        '
        'lblError
        '
        Me.lblError.AutoSize = True
        Me.lblError.ForeColor = System.Drawing.Color.DarkRed
        Me.lblError.Location = New System.Drawing.Point(48, 139)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(39, 13)
        Me.lblError.TabIndex = 3
        Me.lblError.Text = "Label5"
        Me.lblError.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(45, 82)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Contraseña"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(45, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Usuario"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(188, 108)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 2
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'TBContraseña
        '
        Me.TBContraseña.Location = New System.Drawing.Point(106, 82)
        Me.TBContraseña.Name = "TBContraseña"
        Me.TBContraseña.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TBContraseña.Size = New System.Drawing.Size(241, 20)
        Me.TBContraseña.TabIndex = 1
        '
        'tbUsuario
        '
        Me.tbUsuario.Location = New System.Drawing.Point(106, 56)
        Me.tbUsuario.Name = "tbUsuario"
        Me.tbUsuario.Size = New System.Drawing.Size(241, 20)
        Me.tbUsuario.TabIndex = 0
        '
        'PanelLogin
        '
        Me.PanelLogin.Controls.Add(Me.GroupBox2)
        Me.PanelLogin.Location = New System.Drawing.Point(12, 12)
        Me.PanelLogin.Name = "PanelLogin"
        Me.PanelLogin.Size = New System.Drawing.Size(880, 790)
        Me.PanelLogin.TabIndex = 5
        '
        'bttEliminar
        '
        Me.bttEliminar.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttEliminar.FlatAppearance.BorderSize = 0
        Me.bttEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttEliminar.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttEliminar.ForeColor = System.Drawing.Color.Black
        Me.bttEliminar.Image = Global.Stock.My.Resources.Resources.menos
        Me.bttEliminar.Location = New System.Drawing.Point(92, 16)
        Me.bttEliminar.Name = "bttEliminar"
        Me.bttEliminar.Size = New System.Drawing.Size(80, 64)
        Me.bttEliminar.TabIndex = 5
        Me.bttEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.bttEliminar.UseVisualStyleBackColor = True
        '
        'bttEditarProd
        '
        Me.bttEditarProd.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bttEditarProd.FlatAppearance.BorderSize = 0
        Me.bttEditarProd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttEditarProd.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttEditarProd.ForeColor = System.Drawing.Color.Black
        Me.bttEditarProd.Image = Global.Stock.My.Resources.Resources.lapiz
        Me.bttEditarProd.Location = New System.Drawing.Point(6, 16)
        Me.bttEditarProd.Name = "bttEditarProd"
        Me.bttEditarProd.Size = New System.Drawing.Size(80, 64)
        Me.bttEditarProd.TabIndex = 4
        Me.bttEditarProd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.bttEditarProd.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox4.Controls.Add(Me.txtPrecioMi)
        Me.GroupBox4.Controls.Add(Me.txtPrecioMa)
        Me.GroupBox4.Controls.Add(Me.bttGuardarProd)
        Me.GroupBox4.Controls.Add(Me.Label8)
        Me.GroupBox4.Controls.Add(Me.NumCant)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Controls.Add(Me.Label5)
        Me.GroupBox4.Controls.Add(Me.Label6)
        Me.GroupBox4.Controls.Add(Me.tb_colorname)
        Me.GroupBox4.Controls.Add(Me.txtPrecioTa)
        Me.GroupBox4.Controls.Add(Me.bttColor)
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Controls.Add(Me.CBoxTalle)
        Me.GroupBox4.Controls.Add(Me.Button3)
        Me.GroupBox4.Controls.Add(Me.CBoxMarca)
        Me.GroupBox4.Controls.Add(Me.txtDesc)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.Label11)
        Me.GroupBox4.Controls.Add(Me.txtCod)
        Me.GroupBox4.Controls.Add(Me.Label12)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Location = New System.Drawing.Point(6, 639)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(874, 151)
        Me.GroupBox4.TabIndex = 5
        Me.GroupBox4.TabStop = False
        '
        'txtPrecioMi
        '
        Me.txtPrecioMi.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtPrecioMi.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrecioMi.Location = New System.Drawing.Point(626, 15)
        Me.txtPrecioMi.MaxLength = 11
        Me.txtPrecioMi.Name = "txtPrecioMi"
        Me.txtPrecioMi.Size = New System.Drawing.Size(183, 27)
        Me.txtPrecioMi.TabIndex = 54
        '
        'txtPrecioMa
        '
        Me.txtPrecioMa.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtPrecioMa.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrecioMa.Location = New System.Drawing.Point(626, 48)
        Me.txtPrecioMa.MaxLength = 11
        Me.txtPrecioMa.Name = "txtPrecioMa"
        Me.txtPrecioMa.Size = New System.Drawing.Size(183, 27)
        Me.txtPrecioMa.TabIndex = 55
        '
        'bttGuardarProd
        '
        Me.bttGuardarProd.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.bttGuardarProd.BackColor = System.Drawing.Color.FromArgb(CType(CType(206, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.bttGuardarProd.Enabled = False
        Me.bttGuardarProd.FlatAppearance.BorderSize = 0
        Me.bttGuardarProd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(207, Byte), Integer), CType(CType(156, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.bttGuardarProd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(207, Byte), Integer), CType(CType(156, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.bttGuardarProd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttGuardarProd.Font = New System.Drawing.Font("Century Gothic", 11.25!)
        Me.bttGuardarProd.ForeColor = System.Drawing.Color.Black
        Me.bttGuardarProd.Location = New System.Drawing.Point(655, 114)
        Me.bttGuardarProd.Name = "bttGuardarProd"
        Me.bttGuardarProd.Size = New System.Drawing.Size(154, 29)
        Me.bttGuardarProd.TabIndex = 57
        Me.bttGuardarProd.Text = "Guardar"
        Me.bttGuardarProd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttGuardarProd.UseVisualStyleBackColor = False
        '
        'Label8
        '
        Me.Label8.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(285, 83)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(51, 21)
        Me.Label8.TabIndex = 63
        Me.Label8.Text = "Cant"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'NumCant
        '
        Me.NumCant.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.NumCant.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumCant.Location = New System.Drawing.Point(338, 81)
        Me.NumCant.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.NumCant.Name = "NumCant"
        Me.NumCant.Size = New System.Drawing.Size(183, 27)
        Me.NumCant.TabIndex = 53
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(534, 18)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(89, 21)
        Me.Label9.TabIndex = 64
        Me.Label9.Text = "Precio Min"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(532, 85)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(88, 21)
        Me.Label5.TabIndex = 65
        Me.Label5.Text = "Precio Tarj"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(528, 52)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(95, 21)
        Me.Label6.TabIndex = 66
        Me.Label6.Text = "Precio May"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tb_colorname
        '
        Me.tb_colorname.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.tb_colorname.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb_colorname.Location = New System.Drawing.Point(429, 50)
        Me.tb_colorname.Name = "tb_colorname"
        Me.tb_colorname.Size = New System.Drawing.Size(89, 27)
        Me.tb_colorname.TabIndex = 51
        '
        'txtPrecioTa
        '
        Me.txtPrecioTa.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtPrecioTa.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrecioTa.Location = New System.Drawing.Point(626, 81)
        Me.txtPrecioTa.MaxLength = 11
        Me.txtPrecioTa.Name = "txtPrecioTa"
        Me.txtPrecioTa.Size = New System.Drawing.Size(183, 27)
        Me.txtPrecioTa.TabIndex = 56
        '
        'bttColor
        '
        Me.bttColor.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.bttColor.BackColor = System.Drawing.Color.White
        Me.bttColor.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.bttColor.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(207, Byte), Integer), CType(CType(156, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.bttColor.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(207, Byte), Integer), CType(CType(156, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.bttColor.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttColor.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttColor.ForeColor = System.Drawing.Color.Black
        Me.bttColor.Location = New System.Drawing.Point(338, 48)
        Me.bttColor.Name = "bttColor"
        Me.bttColor.Size = New System.Drawing.Size(88, 29)
        Me.bttColor.TabIndex = 46
        Me.bttColor.Text = "Elegir"
        Me.bttColor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttColor.UseVisualStyleBackColor = False
        '
        'Label7
        '
        Me.Label7.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(289, 17)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(45, 21)
        Me.Label7.TabIndex = 61
        Me.Label7.Text = "Talle"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CBoxTalle
        '
        Me.CBoxTalle.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.CBoxTalle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBoxTalle.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBoxTalle.FormattingEnabled = True
        Me.CBoxTalle.Items.AddRange(New Object() {"XS", "S", "M", "L", "XL", "XXL", "XXXL", "28", "30", "32", "34", "36", "38", "40", "41", "42", "43", "44", "46", "48", "50"})
        Me.CBoxTalle.Location = New System.Drawing.Point(338, 15)
        Me.CBoxTalle.Name = "CBoxTalle"
        Me.CBoxTalle.Size = New System.Drawing.Size(183, 29)
        Me.CBoxTalle.TabIndex = 52
        '
        'Button3
        '
        Me.Button3.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Button3.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.Button3.FlatAppearance.BorderSize = 0
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Image = Global.Stock.My.Resources.Resources.mas__2_
        Me.Button3.Location = New System.Drawing.Point(250, 79)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(23, 32)
        Me.Button3.TabIndex = 50
        Me.Button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button3.UseVisualStyleBackColor = True
        '
        'CBoxMarca
        '
        Me.CBoxMarca.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.CBoxMarca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBoxMarca.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CBoxMarca.FormattingEnabled = True
        Me.CBoxMarca.Location = New System.Drawing.Point(90, 80)
        Me.CBoxMarca.Name = "CBoxMarca"
        Me.CBoxMarca.Size = New System.Drawing.Size(154, 29)
        Me.CBoxMarca.TabIndex = 48
        '
        'txtDesc
        '
        Me.txtDesc.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtDesc.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDesc.Location = New System.Drawing.Point(90, 47)
        Me.txtDesc.MaxLength = 50
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.Size = New System.Drawing.Size(183, 27)
        Me.txtDesc.TabIndex = 49
        '
        'Label10
        '
        Me.Label10.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(22, 17)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(68, 21)
        Me.Label10.TabIndex = 58
        Me.Label10.Text = "Código"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(36, 50)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(48, 21)
        Me.Label11.TabIndex = 59
        Me.Label11.Text = "Desc"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCod
        '
        Me.txtCod.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtCod.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCod.Location = New System.Drawing.Point(90, 14)
        Me.txtCod.MaxLength = 50
        Me.txtCod.Name = "txtCod"
        Me.txtCod.Size = New System.Drawing.Size(183, 27)
        Me.txtCod.TabIndex = 47
        '
        'Label12
        '
        Me.Label12.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(22, 83)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(62, 21)
        Me.Label12.TabIndex = 60
        Me.Label12.Text = "Marca"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(289, 53)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(51, 21)
        Me.Label13.TabIndex = 62
        Me.Label13.Text = "Color"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pruebaPRODUCTOS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(902, 814)
        Me.Controls.Add(Me.PanelPrincipal)
        Me.Controls.Add(Me.PanelLogin)
        Me.Name = "pruebaPRODUCTOS"
        Me.Text = "Mantenimiento"
        CType(Me.dgvstock, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.PanelPrincipal.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.PanelLogin.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.NumCant, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents dgvstock As DataGridView
    Friend WithEvents CB_Buscar As ComboBox
    Friend WithEvents txtBuscar As MetroFramework.Controls.MetroTextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents PanelPrincipal As Panel
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents btnAceptar As Button
    Friend WithEvents TBContraseña As TextBox
    Friend WithEvents tbUsuario As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents PanelLogin As Panel
    Friend WithEvents Label4 As Label
    Friend WithEvents lblError As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents bttEliminar As Button
    Friend WithEvents bttEditarProd As Button
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents txtPrecioMi As TextBox
    Friend WithEvents txtPrecioMa As TextBox
    Friend WithEvents bttGuardarProd As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents NumCant As NumericUpDown
    Friend WithEvents Label9 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents tb_colorname As TextBox
    Friend WithEvents txtPrecioTa As TextBox
    Friend WithEvents bttColor As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents CBoxTalle As ComboBox
    Friend WithEvents Button3 As Button
    Friend WithEvents CBoxMarca As ComboBox
    Friend WithEvents txtDesc As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents txtCod As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
End Class
