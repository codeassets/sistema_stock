﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports OfficeOpenXml
Imports OfficeOpenXml.Style
Imports System.IO

Public Class Informes

    Private fechadesde As Date
    Private fechahasta As Date
    Public idcliente As Integer

    Private Sub bttElegirCliente_Click(sender As Object, e As EventArgs) Handles bttElegirCliente.Click

        MessageBox.Show("Esta operación puede tardar unos segundos. Por favor espere.")

        fechadesde = MetroDateTime1.Value
        fechahasta = MetroDateTime2.Value

    End Sub

    Private Sub Informes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MetroDateTime1.Value = Today
        MetroDateTime2.Value = Today
        dtpDesde.Value = Today
        dtpHasta.Value = Today

        CB_Buscar.SelectedIndex = 0
        CB_asc_desc.SelectedIndex = 0
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        Dim f As New ElegirCliente
        f.b = 2

        ''TODO mala practica.
        Try
            f.ShowDialog()
            '' ^ Agregue (Me) para evitar que la ventana desaparezca,
            '' pero tiraba error cuando se cerraba. No tengo idea de como solucionarlo 
            ElegirCliente.b = 2

            Button2.Text = f.DataGridView1.SelectedCells.Item(3).Value.ToString + ", " + f.DataGridView1.SelectedCells.Item(2).Value.ToString
            idcliente = f.DataGridView1.SelectedCells.Item(0).Value.ToString
        Catch ex As Exception

        End Try

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click
        Button2.Text = "Elegir"
        idcliente = Nothing

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub ButtonVentasDesdeHasta_Click(sender As Object, e As EventArgs) Handles ButtonVentasDesdeHasta.Click

        fechadesde = dtpDesde.Value
        fechahasta = dtpHasta.Value

        Dim fd As String = fechadesde.ToShortDateString()
        Dim fh As String = fechahasta.ToShortDateString()

        Using excel As New ExcelPackage()

            Dim workSheet = excel.Workbook.Worksheets.Add("Sheet1")

            Dim v As New DatosVentas

            Try
                Dim dt As DataTable = v.InformeVentasPorFechas(fd, fh)
                workSheet.Cells("A3").LoadFromDataTable(dt, False)

                'posicion donde va a cargar el Total
                Dim pos As Integer = dt.Rows.Count + 3
                workSheet.Cells(pos + 1, 6).Value = "TOTAL"
                workSheet.Cells(pos + 1, 1, pos + 1, 6).Merge = True
                workSheet.Row(pos + 1).Style.Font.Bold = True
                workSheet.Row(pos + 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right
                workSheet.Row(pos + 1).Style.Font.Size = 14
                workSheet.Cells(pos + 1, 7).Formula = "SUM(" & workSheet.Cells(3, 7).Address & ":" & workSheet.Cells(pos - 1, 7).Address & ")"

                workSheet.TabColor = System.Drawing.Color.Black
                workSheet.DefaultRowHeight = 12
                workSheet.Row(1).Height = 20
                workSheet.Row(1).Style.Font.Size = 14
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                workSheet.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                workSheet.Row(1).Style.Font.Bold = True
                workSheet.Cells(1, 1).Value = "Listado de todas las ventas del " & fechadesde & " al " & fechahasta

                workSheet.Cells(2, 1).Value = "Nro. Venta"
                workSheet.Cells(2, 2).Value = "Fecha"
                workSheet.Cells(2, 3).Value = "Cliente"
                workSheet.Cells(2, 4).Value = "Tipo"
                workSheet.Cells(2, 5).Value = "Pago"
                workSheet.Cells(2, 6).Value = "Descuento"
                workSheet.Cells(2, 7).Value = "Total"

                workSheet.Cells("A1:G1").Merge = True

                workSheet.Column(1).AutoFit()
                workSheet.Column(1).Style.Font.Bold = True
                workSheet.Column(2).Style.Numberformat.Format = "dd-mm-yyyy"
                workSheet.Column(2).AutoFit()
                workSheet.Column(3).AutoFit()
                workSheet.Column(4).AutoFit()
                workSheet.Column(5).AutoFit()
                workSheet.Column(6).Style.Numberformat.Format = "$###,###,##0.00"
                workSheet.Column(6).AutoFit()
                workSheet.Column(7).Style.Numberformat.Format = "$###,###,##0.00"
                workSheet.Column(7).AutoFit()
                workSheet.Column(7).Style.Font.Bold = True

                workSheet.Cells("A1:G1").Style.Font.Bold = True
                workSheet.Cells("A2:G2").Style.Font.Bold = True

                'acomodar el sheet entero
                workSheet.Cells(workSheet.Dimension.Address).AutoFitColumns()

                Dim a As String = "Listado Ventas desde " & fechadesde & " hasta " & fechahasta & ".xlsx"
                Dim excelName As String = a.Replace("/", "-")


                Try
                    'create a SaveFileDialog instance with some properties
                    Dim saveFileDialog1 As New SaveFileDialog()
                    saveFileDialog1.Title = "Save Excel sheet"
                    saveFileDialog1.Filter = "Excel files|*.xlsx|All files|*.*"
                    saveFileDialog1.FileName = excelName
                    'check if user clicked the save button
                    If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                        'Get the FileInfo
                        Dim fi As New FileInfo(saveFileDialog1.FileName)
                        excel.SaveAs(fi)
                        'write the file to the disk
                    End If

                Catch ex As Exception
                    MessageBox.Show("Surgió un problema al guardar el archivo Excel. Detalle: " & ex.Message.ToString)
                End Try
            Catch ex As Exception
                MessageBox.Show("Surgió un problema al recuperar los datos de ventas. Detalle: " & ex.Message.ToString)
            End Try

        End Using
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Try
            Using excel As New ExcelPackage()

                Dim workSheet = excel.Workbook.Worksheets.Add("Sheet1")

                Dim prod As New DatosProductos
                Dim orden As String
                Dim ascdesc As String
                If CB_Buscar.SelectedIndex = 0 Then 'codigo
                    orden = "Codigo"
                ElseIf CB_Buscar.SelectedIndex = 1 Then 'talle
                    orden = "Talle"
                ElseIf CB_Buscar.SelectedIndex = 2 Then 'marca
                    orden = "Marca"
                ElseIf CB_Buscar.SelectedIndex = 3 Then 'color
                    orden = "Color"
                ElseIf CB_Buscar.SelectedIndex = 4 Then 'descripcion
                    orden = "Descripcion"
                ElseIf CB_Buscar.SelectedIndex = 5 Then 'preciomin
                    orden = "PrecioMinorista"
                ElseIf CB_Buscar.SelectedIndex = 6 Then 'preciomay
                    orden = "PrecioMayorista"
                ElseIf CB_Buscar.SelectedIndex = 7 Then 'preciotarj
                    orden = "PrecioTarjeta"
                ElseIf CB_Buscar.SelectedIndex = 8 Then 'cantidad
                    orden = "Cantidad"
                Else
                    orden = "IdProducto"
                End If

                If CB_asc_desc.SelectedIndex = 0 Then 'ascendente
                    ascdesc = "ASC"
                Else
                    ascdesc = "DESC"
                End If

                Dim dataTable As DataTable = prod.ReporteTodosProductos(orden, ascdesc)
                workSheet.Cells("A3").LoadFromDataTable(dataTable, False)

                workSheet.TabColor = System.Drawing.Color.Black
                workSheet.DefaultRowHeight = 12
                workSheet.Row(1).Height = 20
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
                workSheet.Row(1).Style.Font.Bold = True
                workSheet.Cells(1, 1).Value = "Listado de Todos los productos a la fecha " & Today.Date & " Ordenados por " & orden & " " & ascdesc
                workSheet.Cells(2, 1).Value = "NroProducto"
                workSheet.Cells(2, 2).Value = "Codigo"
                workSheet.Cells(2, 3).Value = "Descripcion"
                workSheet.Cells(2, 4).Value = "Marca"
                workSheet.Cells(2, 5).Value = "Talle"
                workSheet.Cells(2, 6).Value = "Color"
                workSheet.Cells(2, 7).Value = "Precio Minorista"
                workSheet.Cells(2, 8).Value = "Precio Mayorista"
                workSheet.Cells(2, 9).Value = "Precio Tarjeta"
                workSheet.Cells(2, 10).Value = "Cantidad"


                workSheet.Cells("A1:J1").Merge = True

                workSheet.Column(1).AutoFit()
                workSheet.Column(1).Style.Font.Bold = True
                workSheet.Column(2).AutoFit()
                workSheet.Column(2).Style.Locked = False
                workSheet.Column(3).AutoFit()
                workSheet.Column(3).Style.Locked = False
                workSheet.Column(4).AutoFit()
                workSheet.Column(4).Style.Locked = False
                workSheet.Column(5).AutoFit()
                workSheet.Column(5).Style.Locked = False
                workSheet.Column(6).Style.Numberformat.Format = "$###,###,##0.00"
                workSheet.Column(6).AutoFit()
                workSheet.Column(6).Style.Locked = False
                workSheet.Column(7).Style.Numberformat.Format = "$###,###,##0.00"
                workSheet.Column(7).AutoFit()
                workSheet.Column(7).Style.Locked = False
                workSheet.Column(8).Style.Numberformat.Format = "$###,###,##0.00"
                workSheet.Column(8).AutoFit()
                workSheet.Column(8).Style.Locked = False
                workSheet.Column(9).AutoFit()
                workSheet.Column(9).Style.Locked = False
                workSheet.Column(10).AutoFit()
                workSheet.Column(10).Style.Locked = False

                workSheet.Cells("A1:J1").Style.Locked = True
                workSheet.Cells("A1:J1").Style.Font.Bold = True
                workSheet.Cells("A2:J2").Style.Locked = True
                workSheet.Cells("A2:J2").Style.Font.Bold = True
                workSheet.Protection.IsProtected = True

                Dim a As String = "Listado Productos " & Today.Date & " Orden_" & orden & "_" & ascdesc & ".xlsx"
                Dim excelName As String = a.Replace("/", "-")


                'create a SaveFileDialog instance with some properties
                Dim saveFileDialog1 As New SaveFileDialog()
                saveFileDialog1.Title = "Save Excel sheet"
                saveFileDialog1.Filter = "Excel files|*.xlsx|All files|*.*"
                saveFileDialog1.FileName = excelName
                'check if user clicked the save button
                If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                    'Get the FileInfo
                    Dim fi As New FileInfo(saveFileDialog1.FileName)
                    'write the file to the disk
                    excel.SaveAs(fi)

                End If

            End Using
        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema al crear el Listado de Productos. Detalle: " & ex.Message.ToString)
        End Try
    End Sub
End Class