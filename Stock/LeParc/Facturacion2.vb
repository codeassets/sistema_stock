﻿Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Text
Imports Stock.WSFE.HOMO
Imports QRCoder

Public Class Facturacion2

    ' para el constructor
    Dim fila As New DataGridViewRow

    ' para mandar  al webservice
    Dim ptovta As Integer = 9 ' punto de venta
    Dim cbtetipo As Integer  ' tipo de comprobante
    Dim cbtetipo_desc As String
    Dim concepto As Integer = 1 ' concepto: 1 prod 2 serv 3 prod y serv
    Dim concepto_desc As String = "Producto"
    Dim doctipo As Integer  ' tipo de documento
    Dim doctipo_desc As String
    Dim docnro As Int64 ' numero de documento
    Dim CbteDesde As Integer
    Dim CbteHasta As Integer
    Dim cbtefch As String ' fecha de comprobante: yyyymmdd
    Dim importeneto As Double ' importe neto no gravado
    Dim importeiva As Double = 0 ' suma de importes de IVA
    Dim importetotal As Double ' importe total del comprobante
    Dim iva As String = "0" ' 
    Dim imptotalconc As Double ' importe neto no gravado
    Dim impopex As Double ' importe exento. Debe ser menor o igual al importe total y no debe ser menor a 0
    Dim imptrib As Double ' suma de los importes del array de tributos
    Dim idiva As Integer ' codigo de tipo iva
    Dim baseimp As Double ' base imponible para la determinación de la alicuota
    Dim estado As String

    ' para AFIP
    Private l As LoginAFIP
    Property Login As LoginAFIP
    Property TiposComprobantes As CbteTipoResponse
    Property TipoConceptos As ConceptoTipoResponse
    Property TipoDoc As DocTipoResponse
    Property Monedas As MonedaResponse
    Property puntosventa As FEPtoVentaResponse
    Property TiposIVA As IvaTipoResponse
    Property opcionales As OpcionalTipoResponse
    Property authRequest As FEAuthRequest
    Private url As String

    Private Function getServicio() As Service
        Dim s As New Service
        s.Url = My.Settings.url_service
        Return s
    End Function

    Public Function CalcChecksum(S As String) As Byte
        Dim chk As Byte ' Final Checksum
        Dim T As Long   ' Char-pointer...

        ' Step 1
        chk = Asc(Mid(S, 1, 2))    ' Get first Char value

        ' Initialize counter
        T = 1
        While T < Len(S)

            ' Step 2
            chk = chk And 127   ' Bitwise operation. Clear bit 7 (127 -> 0111 1111)

            ' Step 3
            chk = chk Or 64     ' Bitwise operation. Set bit 6 (64 -> 01000000)

            ' Increase counter...
            T = T + 1

            ' Step 4
            chk = Asc(Mid(S, T, 1)) + chk
        End While

        CalcChecksum = chk
    End Function

    'Funcion para que no se coloque mas de una coma en el txtbox
    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
        Dim cnt As Integer = 0
        For Each c As Char In value
            If c = ch Then
                cnt += 1
            End If
        Next
        Return cnt
    End Function

    Sub New(_fila As DataGridViewRow)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        fila = _fila

    End Sub

    Private Sub Facturacion2_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        'verificar login
        l = New LoginAFIP(My.Settings.def_serv, My.Settings.def_url, My.Settings.def_cert, My.Settings.def_pass)
        l.hacerLogin()

        cboxComprobante.SelectedIndex = cboxComprobante.FindStringExact(fila.Cells("CbteTipo_desc").Value)

        dtpFecha.Value = fila.Cells("CbteFch").Value
        txtDocumento.Text = fila.Cells("DocNro").Value

        ' cargar los detalles en la tabla resumen
        Dim detalle As New DatosFacturas
        detalle._IdFactura = fila.Cells("IdFactura").Value
        dgvResumen.DataSource = detalle.ObtenerDetalles

        AjustarTabla()

        ' completar totales
        importeneto = fila.Cells("ImpNeto").Value
        importetotal = fila.Cells("ImpTotal").Value
        importeiva = fila.Cells("ImpIVA").Value

        'var para mandar al afip, por defecto factura B
        cbtetipo = fila.Cells("CbteTipo").Value
        cbtetipo_desc = fila.Cells("CbteTipo_desc").Value
        concepto = fila.Cells("Concepto").Value
        concepto_desc = fila.Cells("Concepto_desc").Value
        doctipo = fila.Cells("DocTipo").Value
        doctipo_desc = fila.Cells("DocTipo_desc").Value
        docnro = fila.Cells("DocNro").Value
        cbtefch = fila.Cells("CbteFch").Value
        imptotalconc = 0
        impopex = 0
        imptrib = 0
        idiva = fila.Cells("AlicIva_Id").Value
        baseimp = fila.Cells("AlicIva_BaseImp").Value
        estado = "Facturado"

        If cboxComprobante.SelectedIndex = 0 Then
            ' factura A
            lblIVA.Text = "IVA: 21%"
        Else
            'factura B
            lblIVA.Text = "IVA: 21%"
        End If

        lblImporteIVA.Text = "Importe IVA: $ " & fila.Cells("ImpIVA").Value
        lblImporteNeto.Text = "Importe Neto: $ " & fila.Cells("ImpNeto").Value
        lblTotal.Text = "Total: $ " & fila.Cells("ImpTotal").Value



    End Sub

    Public Sub AjustarTabla()

        dgvResumen.Columns("IdFactura").Visible = False
        dgvResumen.Columns("IdDetalle").Visible = False
        dgvResumen.Columns("IdProducto").Visible = False
        dgvResumen.Columns("Total").Visible = False

        dgvResumen.Columns("PrecioUnitario").HeaderText = "Precio Unit."
        dgvResumen.Columns("Descripcion").HeaderText = "Prod."
        dgvResumen.Columns("Cantidad").HeaderText = "Cant."
        dgvResumen.Columns("Subtotal").HeaderText = "Subt."


    End Sub


    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub txtPago_TextChanged(sender As Object, e As EventArgs) Handles txtPago.TextChanged
        If txtPago.Text = "" OrElse txtPago.Text = 0 Then
            txtVuelto.Text = 0
            txtPago.Text = 0
            txtPago.SelectAll()
        Else
            txtVuelto.Text = txtPago.Text - importetotal
        End If
    End Sub

    Private Sub txtPago_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPago.KeyPress
        'Verifica que solo se ingresen NUMEROS ENTEROS y la "coma"
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                If Asc(e.KeyChar) = 44 Then 'si quieres q sea "PUNTO" pone 46
                    Dim count As Integer = CountCharacter(txtPago.Text, e.KeyChar)
                    If count <> 0 Then
                        e.Handled = True
                    End If
                Else
                    e.Handled = True
                End If
            End If
        End If
    End Sub

    Private Sub txtPago_Click(sender As Object, e As EventArgs) Handles txtPago.Click
        txtPago.SelectAll()
    End Sub

    Private Sub txtDocumento_TextChanged(sender As Object, e As EventArgs) Handles txtDocumento.TextChanged
        If Trim(txtDocumento.Text) = "" Then
            txtDocumento.Text = "99999999"
        End If
    End Sub

    Private Sub bttCobrar_Click(sender As Object, e As EventArgs) Handles bttCobrar.Click
        Net.ServicePointManager.SecurityProtocol = Net.SecurityProtocolType.Tls12
        Try

            Dim f As New DatosFacturas

#Region "Conexion a AFIP y obtencion del CAE"
            authRequest = New FEAuthRequest()
            authRequest.Cuit = My.Settings.def_cuit
            authRequest.Sign = Login.Sign
            authRequest.Token = Login.Token

            Dim service As WSFE.HOMO.Service = getServicio()
            service.ClientCertificates.Add(Login.certificado)


            Dim req As New FECAERequest
            Dim cab As New FECAECabRequest
            Dim det As New FECAEDetRequest

            cab.CantReg = 1
            cab.PtoVta = ptovta
            cab.CbteTipo = cbtetipo
            req.FeCabReq = cab

            With det

                .Concepto = concepto
                .DocTipo = doctipo
                .DocNro = txtDocumento.Text

                Dim lastRes As FERecuperaLastCbteResponse = service.FECompUltimoAutorizado(authRequest, ptovta, cbtetipo)
                Dim last As Integer = lastRes.CbteNro

                CbteDesde = last + 1
                CbteHasta = last + 1
                .CbteDesde = last + 1
                .CbteHasta = last + 1

                .CbteFch = dtpFecha.Value.ToString("yyyyMMdd")

                .ImpNeto = importeneto
                .ImpIVA = importeiva
                .ImpTotal = importetotal

                .ImpTotConc = 0
                .ImpOpEx = 0
                .ImpTrib = 0

                .MonId = "PES"
                .MonCotiz = 1

                Dim alicuota As New AlicIva
                alicuota.Id = idiva
                alicuota.BaseImp = baseimp
                alicuota.Importe = importeiva

                .Iva = {alicuota}


            End With

            req.FeDetReq = {det}

            'con esta linea de codigo obtenemos el CAE
            Dim r = service.FECAESolicitar(authRequest, req)
#End Region

#Region "Imprimo r que seria el resultado de obtener el CAE"

            Dim m As String = "Estado: " & r.FeCabResp.Resultado & vbCrLf
            m &= "Estado Esp: " & r.FeDetResp(0).Resultado
            m &= vbCrLf
            m &= "CAE: " & r.FeDetResp(0).CAE
            m &= vbCrLf
            m &= "Vto: " & r.FeDetResp(0).CAEFchVto
            m &= vbCrLf
            m &= "Desde-Hasta: " & r.FeDetResp(0).CbteDesde & "-" & r.FeDetResp(0).CbteHasta
            m &= vbCrLf

#End Region

#Region "Imprimimos las observaciones y errores de la variable r con la que obtuvimos el CAE"

            Dim mensaje_obs_errores_eventos As String
            If r.FeDetResp(0).Observaciones IsNot Nothing Then
                For Each o In r.FeDetResp(0).Observaciones
                    m &= String.Format("Obs: {0} ({1})", o.Msg, o.Code) & vbCrLf
                    mensaje_obs_errores_eventos &= String.Format("Obs: {0} ({1})", o.Msg, o.Code) & vbCrLf
                Next
            End If
            If r.Errors IsNot Nothing Then
                For Each er In r.Errors
                    m &= String.Format("Er: {0}: {1}", er.Code, er.Msg) & vbCrLf
                    mensaje_obs_errores_eventos &= String.Format("Er: {0}: {1}", er.Code, er.Msg) & vbCrLf
                Next
            End If
            If r.Events IsNot Nothing Then
                For Each ev In r.Events
                    m &= String.Format("Ev: {0}: {1}", ev.Code, ev.Msg) & vbCrLf
                    mensaje_obs_errores_eventos &= String.Format("Ev: {0}: {1}", ev.Code, ev.Msg) & vbCrLf
                Next
            End If
#End Region

#Region "Armo el codigo y genero el codigo QR"


            'codigo QR
            Dim barcodestring As String = ""
            barcodestring = "Estado: " & r.FeCabResp.Resultado & vbCrLf
            barcodestring &= "CAE: " & r.FeDetResp(0).CAE
            barcodestring &= vbCrLf
            barcodestring &= "Vto: " & r.FeDetResp(0).CAEFchVto
            barcodestring &= vbCrLf
            barcodestring &= "Desde-Hasta: " & r.FeDetResp(0).CbteDesde & "-" & r.FeDetResp(0).CbteHasta
            barcodestring &= vbCrLf
            barcodestring &= "Para: " & txtDocumento.Text
            barcodestring &= vbCrLf
            barcodestring &= "Tipo Emision: " & "CAE"
            barcodestring &= vbCrLf
            barcodestring &= "Total: " & importetotal
            barcodestring &= vbCrLf

            barcodestring &= "CRC32: " & CalcChecksum(barcodestring)
            barcodestring &= vbCrLf



            If r.FeDetResp(0).Observaciones IsNot Nothing Then
                For Each o In r.FeDetResp(0).Observaciones
                    barcodestring &= String.Format("Obs: {0} ({1})", o.Msg, o.Code) & vbCrLf
                Next
            End If

            'PicBarCodeFB.BackgroundImage = Code128(barcodestring, "A")
            Dim gen As New QRCodeGenerator
            Dim data = gen.CreateQrCode(m, QRCodeGenerator.ECCLevel.Q)

            Dim code As New QRCode(data)
            'PB_QR_B.BackgroundImage = code.GetGraphic(6)

            'lblCodQR_B.Text = barcodestring
#End Region

            'asigno valores que faltaban

            'verificar si el estado de r es a(aprobado) o r(rechazado)
            If r.FeDetResp(0).Resultado = "R" Then

                f._Mensaje = mensaje_obs_errores_eventos
                MessageBox.Show("Se rechazó la factura. Detalle: " & vbCrLf & mensaje_obs_errores_eventos, "", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            Else
                f._Estado = "Facturado"
                f._CbeteDesde = CbteDesde
                f._CbteHasta = CbteHasta
                f._CbteFch = dtpFecha.Value
                f._DocNro = txtDocumento.Text
                f._CAE = r.FeDetResp(0).CAE
                f._CAE_vto = r.FeDetResp(0).CAEFchVto
                f._Mensaje = ""

                ' Actualizar factura
                f._IdFactura = fila.Cells("IdFactura").Value
                f.ActualizarFactura()

                MessageBox.Show("Facturado exitosamente. Detalle: " & vbCrLf & mensaje_obs_errores_eventos, "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)

            End If

            Me.DialogResult = DialogResult.OK
            Me.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub bttElegirCliente_Click(sender As Object, e As EventArgs) Handles bttElegirCliente.Click

        Dim f As New ElegirCliente

        Try
            If f.ShowDialog() = DialogResult.OK Then

                txtDocumento.Text = f.DataGridView1.SelectedCells.Item(1).Value.ToString

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try

    End Sub

    Private Sub dtpFecha_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecha.ValueChanged
        cbtefch = dtpFecha.Value.ToString("yyyyMMdd")
    End Sub
End Class