﻿Imports System.Drawing.Printing
Imports System.IO
Imports System.Security.Cryptography.X509Certificates
Imports System.Text
Imports Stock.WSFE.HOMO
Imports QRCoder

Public Class Facturación

    ' para el constructor
    Dim idventa As Int64
    Dim documentocliente As Int64
    Dim total As Double
    Dim descuento As Double
    Dim tipopago As String

    'pictureboxQR
    Dim pb_codQR As New PictureBox

    ' Otras
    Dim nombrecliente As String = "1"
    Dim telefonocliente As String = "1"
    Dim direccioncliente As String = "1"

    ' para mandar  al webservice
    Dim ptovta As Integer = 9 ' punto de venta
    Dim cbtetipo As Integer  ' tipo de comprobante
    Dim cbtetipo_desc As String
    Dim concepto As Integer = 1 ' concepto: 1 prod 2 serv 3 prod y serv
    Dim concepto_desc As String = "Producto"
    Dim doctipo As Integer  ' tipo de documento
    Dim doctipo_desc As String
    Dim docnro As Int64 ' numero de documento
    Dim CbteDesde As Integer
    Dim CbteHasta As Integer
    Dim cbtefch As String ' fecha de comprobante: yyyymmdd
    Dim importeneto As Double ' importe neto no gravado
    Dim importeiva As Double = 0 ' suma de importes de IVA
    Dim importetotal As Double ' importe total del comprobante
    Dim iva As String = "0" ' 
    Dim imptotalconc As Double ' importe neto no gravado
    Dim impopex As Double ' importe exento. Debe ser menor o igual al importe total y no debe ser menor a 0
    Dim imptrib As Double ' suma de los importes del array de tributos
    Dim idiva As Integer ' codigo de tipo iva
    Dim baseimp As Double ' base imponible para la determinación de la alicuota
    Dim estado As String

    ' para AFIP
    Private l As LoginAFIP
    Property Login As LoginAFIP
    Property TiposComprobantes As CbteTipoResponse
    Property TipoConceptos As ConceptoTipoResponse
    Property TipoDoc As DocTipoResponse
    Property Monedas As MonedaResponse
    Property puntosventa As FEPtoVentaResponse
    Property TiposIVA As IvaTipoResponse
    Property opcionales As OpcionalTipoResponse
    Property authRequest As FEAuthRequest
    Private url As String

    Private Function getServicio() As Service
        Dim s As New Service
        s.Url = My.Settings.url_service
        Return s
    End Function

    Public Function CalcChecksum(S As String) As Byte
        Dim chk As Byte ' Final Checksum
        Dim T As Long   ' Char-pointer...

        ' Step 1
        chk = Asc(Mid(S, 1, 2))    ' Get first Char value

        ' Initialize counter
        T = 1
        While T < Len(S)

            ' Step 2
            chk = chk And 127   ' Bitwise operation. Clear bit 7 (127 -> 0111 1111)

            ' Step 3
            chk = chk Or 64     ' Bitwise operation. Set bit 6 (64 -> 01000000)

            ' Increase counter...
            T = T + 1

            ' Step 4
            chk = Asc(Mid(S, T, 1)) + chk
        End While

        CalcChecksum = chk
    End Function

    'Funcion para que no se coloque mas de una coma en el txtbox
    Public Function CountCharacter(ByVal value As String, ByVal ch As Char) As Integer
        Dim cnt As Integer = 0
        For Each c As Char In value
            If c = ch Then
                cnt += 1
            End If
        Next
        Return cnt
    End Function

    Sub New(_titulo As String, _idventa As Int64, _documentocliente As Int64, _total As Double, _descuento As Double, _tipopago As String, _nombrecliente As String, _direccioncliente As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        idventa = _idventa
        documentocliente = _documentocliente
        nombrecliente = _nombrecliente
        direccioncliente = _direccioncliente
        total = _total
        descuento = _descuento
        tipopago = _tipopago

        lblTitulo.Text = _titulo
        txtDocumento.Text = documentocliente
    End Sub

    Private Sub Facturación_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        'verificar login
        Net.ServicePointManager.SecurityProtocol = Net.SecurityProtocolType.Tls12
        l = New LoginAFIP(My.Settings.def_serv, My.Settings.def_url, My.Settings.def_cert, My.Settings.def_pass)
        l.hacerLogin()


        cboxComprobante.SelectedIndex = 0

        ' cargar los detalles en la tabla resumen
        Dim venta As New DatosVentas
        venta.IdVentas_ = idventa
        dgvResumen.DataSource = venta.ObtenerDetalles
        dgvResumen.Columns("IdDetalle").Visible = False
        dgvResumen.Columns("IdProducto").Visible = False
        dgvResumen.Columns("CodigoColor").Visible = False
        dgvResumen.Columns("IdVenta").Visible = False
        dgvResumen.Columns("Total").Visible = False

        dgvResumen.Columns("Descripcion").HeaderText = "Prod."
        dgvResumen.Columns("PrecioUnitario").HeaderText = "Precio Unit."
        dgvResumen.Columns("Cantidad").HeaderText = "Cant."
        dgvResumen.Columns("Subtotal").HeaderText = "Subt."

        ' completar totales
        Dim mul As Decimal = FormatNumber(1 + (21 / 100), 2)
        importeneto = FormatNumber(importetotal / mul, 2)
        importetotal = total


        'var para mandar al afip, por defecto factura B
        cbtetipo = 6
        cbtetipo_desc = "Factura B"
        concepto = 1
        concepto_desc = "Producto"
        doctipo = 96
        doctipo_desc = "DNI"
        docnro = documentocliente
        cbtefch = dtpFecha.Value.ToString("yyyyMMdd")
        imptotalconc = 0
        impopex = 0
        imptrib = 0
        idiva = 5
        baseimp = total
        importeiva = FormatNumber(importetotal - importeneto, 2)
        estado = "No Facturado"

        lblImporteNeto.Text = "Importe Neto: $ " & importeneto
        lblTotal.Text = "Total: $ " & importetotal

    End Sub

    Private Sub cboxComprobante_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboxComprobante.SelectedIndexChanged

        importetotal = total
        Dim mul As Decimal = FormatNumber(1 + (21 / 100), 2)
        importeneto = FormatNumber(importetotal / mul, 2)
        importeiva = FormatNumber(importetotal - importeneto, 2)


        If cboxComprobante.SelectedIndex = 0 Then ' ticket comun


            'var para mandar al afip, por defecto factura B
            cbtetipo = 6
            cbtetipo_desc = "Factura B"
            concepto = 1
            concepto_desc = "Producto"
            doctipo = 96
            doctipo_desc = "DNI"
            docnro = txtDocumento.Text
            cbtefch = dtpFecha.Value.ToString("yyyyMMdd")
            imptotalconc = 0
            impopex = 0
            imptrib = 0
            idiva = 5
            baseimp = importeneto
            estado = "No Facturado"


            lblIVA.Text = "IVA: 21%"
            lblImporteIVA.Text = "Importe IVA: $ " & importeiva
            lblImporteNeto.Text = "Importe Neto: $ " & importeneto
            lblTotal.Text = "Total: $ " & importetotal

        ElseIf cboxComprobante.SelectedIndex = 1 Then ' factura A

            'var para mandar al afip, por defecto factura B
            cbtetipo = 1
            cbtetipo_desc = "Factura A"
            concepto = 1
            concepto_desc = "Producto"
            doctipo = 80
            doctipo_desc = "CUIT"
            docnro = txtDocumento.Text
            cbtefch = dtpFecha.Value.ToString("yyyyMMdd")
            imptotalconc = 0
            impopex = 0
            imptrib = 0
            idiva = 5
            baseimp = importeneto
            estado = "Facturado"

            lblIVA.Text = "IVA: 21%"
            lblImporteIVA.Text = "Importe IVA: $ " & importeiva
            lblImporteNeto.Text = "Importe Neto: $ " & importeneto
            lblTotal.Text = "Total: $ " & importetotal

        ElseIf cboxComprobante.SelectedIndex = 2 Then ' factura B

            'var para mandar al afip, por defecto factura B
            cbtetipo = 6
            cbtetipo_desc = "Factura B"
            concepto = 1
            concepto_desc = "Producto"
            doctipo = 96
            doctipo_desc = "DNI"
            docnro = txtDocumento.Text
            cbtefch = dtpFecha.Value.ToString("yyyyMMdd")
            imptotalconc = 0
            impopex = 0
            imptrib = 0
            idiva = 5
            baseimp = importeneto
            estado = "Facturado"


            lblIVA.Text = "IVA: 21%"
            lblImporteIVA.Text = "Importe IVA: $ " & importeiva
            lblImporteNeto.Text = "Importe Neto: $ " & importeneto
            lblTotal.Text = "Total: $ " & importetotal

        End If

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub



    Private Sub bttCobrar_Click(sender As Object, e As EventArgs) Handles bttCobrar.Click

        Net.ServicePointManager.SecurityProtocol = Net.SecurityProtocolType.Tls12

        Try
            Dim f As New DatosFacturas
            Dim idfactura As Long
            ''Asignar valores
            f._IdVenta = idventa
            f._PtoVta = ptovta
            f._CbteTipo = cbtetipo
            f._CbteTipo_desc = cbtetipo_desc
            f._Concepto = concepto
            f._Concepto_desc = concepto_desc
            f._DocTipo = doctipo
            f._DocTipo_desc = doctipo_desc
            f._DocNro = txtDocumento.Text
            'f._CbeteDesde = CbteDesde
            'f._CbteHasta = CbteHasta
            f._CbteFch = dtpFecha.Value
            f._ImpTotal = importetotal
            f._ImpTotConc = 0
            f._ImpNeto = importeneto
            f._ImpOpEx = 0
            f._ImpTrib = 0
            f._ImpIVA = importeiva
            f._MonId = "PES"
            f._MonCotiz = 1
            f._AlicIva_Id = idiva
            f._AlicIva_BaseImp = baseimp
            f._AlicIva_Importe = importeiva
            f._TipoPago = tipopago
            'f._CAE = r.FeDetResp(0).CAE
            'f._CAE_vto = r.FeDetResp(0).CAEFchVto

            If cboxComprobante.SelectedIndex = 0 Then 'guardar en bd como ticket comun
                f._Estado = "No Facturado"
                f._CbeteDesde = Nothing
                f._CbteHasta = Nothing
                f._CAE = Nothing
                f._CAE_vto = Nothing
                f._Mensaje = Nothing
                MessageBox.Show("Factura guardada.", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)

                ' Imprimir tickets

                If cbTicket.Checked = True Then
                    PrintDocumentFactura.PrinterSettings.PrinterName = My.Settings.Printer
                    PrintDocumentFactura.Print()
                End If

                If cbTicketCambio.Checked = True Then
                    PrintDocumentTicketCambio.PrinterSettings.PrinterName = My.Settings.Printer
                    PrintDocumentTicketCambio.Print()
                End If

            Else
                f._Estado = "Facturado"

#Region "Conexion a AFIP y obtencion del CAE"
                authRequest = New FEAuthRequest()
                authRequest.Cuit = My.Settings.def_cuit
                authRequest.Sign = Login.Sign
                authRequest.Token = Login.Token

                Dim service As WSFE.HOMO.Service = getServicio()
                service.ClientCertificates.Add(Login.certificado)


                Dim req As New FECAERequest
                Dim cab As New FECAECabRequest
                Dim det As New FECAEDetRequest

                cab.CantReg = 1
                cab.PtoVta = ptovta
                cab.CbteTipo = cbtetipo
                req.FeCabReq = cab

                With det

                    .Concepto = concepto
                    .DocTipo = doctipo
                    .DocNro = txtDocumento.Text

                    Dim lastRes As FERecuperaLastCbteResponse = service.FECompUltimoAutorizado(authRequest, ptovta, cbtetipo)
                    Dim last As Integer = lastRes.CbteNro

                    CbteDesde = last + 1
                    CbteHasta = last + 1
                    .CbteDesde = last + 1
                    .CbteHasta = last + 1

                    .CbteFch = cbtefch

                    .ImpNeto = importeneto
                    .ImpIVA = importeiva
                    .ImpTotal = importetotal

                    .ImpTotConc = 0
                    .ImpOpEx = 0
                    .ImpTrib = 0

                    .MonId = "PES"
                    .MonCotiz = 1

                    Dim alicuota As New AlicIva
                    alicuota.Id = idiva
                    alicuota.BaseImp = baseimp
                    alicuota.Importe = importeiva

                    .Iva = {alicuota}


                End With

                req.FeDetReq = {det}

                'con esta linea de codigo obtenemos el CAE
                Dim r = service.FECAESolicitar(authRequest, req)
#End Region

#Region "Imprimo r que seria el resultado de obtener el CAE"


                Dim m As String = "Estado: " & r.FeCabResp.Resultado & vbCrLf
                m &= "Estado Esp: " & r.FeDetResp(0).Resultado
                m &= vbCrLf
                m &= "CAE: " & r.FeDetResp(0).CAE
                m &= vbCrLf
                m &= "Vto: " & r.FeDetResp(0).CAEFchVto
                m &= vbCrLf
                m &= "Desde-Hasta: " & r.FeDetResp(0).CbteDesde & "-" & r.FeDetResp(0).CbteHasta
                m &= vbCrLf

#End Region

#Region "Imprimimos las observaciones y errores de la variable r con la que obtuvimos el CAE"

                Dim mensaje_obs_errores_eventos As String
                If r.FeDetResp(0).Observaciones IsNot Nothing Then
                    For Each o In r.FeDetResp(0).Observaciones
                        m &= String.Format("Obs: {0} ({1})", o.Msg, o.Code) & vbCrLf
                        mensaje_obs_errores_eventos &= String.Format("Obs: {0} ({1})", o.Msg, o.Code) & "; "
                    Next
                End If
                If r.Errors IsNot Nothing Then
                    For Each er In r.Errors
                        m &= String.Format("Er: {0}: {1}", er.Code, er.Msg) & vbCrLf
                        mensaje_obs_errores_eventos &= String.Format("Er: {0}: {1}", er.Code, er.Msg) & "; "
                    Next
                End If
                If r.Events IsNot Nothing Then
                    For Each ev In r.Events
                        m &= String.Format("Ev: {0}: {1}", ev.Code, ev.Msg) & vbCrLf
                        mensaje_obs_errores_eventos &= String.Format("Ev: {0}: {1}", ev.Code, ev.Msg) & "; "
                    Next
                End If
#End Region

#Region "para codigo de barras"


                ''Para codigo de Barras
                'Dim barcodestring As String = ""
                'barcodestring = r.FeCabResp.Resultado
                'barcodestring &= r.FeDetResp(0).CAE
                'barcodestring &= r.FeDetResp(0).CAEFchVto
                'barcodestring &= r.FeDetResp(0).CbteDesde & "-" & r.FeDetResp(0).CbteHasta
                'barcodestring &= documentocliente
                'barcodestring &= "CAE"
                'barcodestring &= importetotal
#End Region

#Region "Armo el codigo y genero el codigo QR"
                ' Codigo QR
                Dim qrcodestring As String = String.Concat(authRequest.Cuit,
                                      cbtetipo.ToString("00"),
                                      ptovta.ToString("0000"),
                                      r.FeDetResp(0).CAE,
                                      r.FeDetResp(0).CAEFchVto)
                qrcodestring &= CalcChecksum(qrcodestring)

                'If r.FeDetResp(0).Observaciones IsNot Nothing Then
                '    For Each o In r.FeDetResp(0).Observaciones
                '        barcodestring &= String.Format("Obs: {0} ({1})", o.Msg, o.Code) & vbCrLf
                '    Next
                'End If

                Dim gen As New QRCodeGenerator
                Dim data = gen.CreateQrCode(qrcodestring, QRCodeGenerator.ECCLevel.Q)

                Dim code As New QRCode(data)
                pb_codQR.BackgroundImage = code.GetGraphic(6)

#End Region

                'asigno valores que faltaban

                'verificar si el estado de r es a(aprobado) o r(rechazado)
                If r.FeDetResp(0).Resultado = "R" Then
                    f._Estado = "No Facturado"
                    f._CbeteDesde = Nothing
                    f._CbteHasta = Nothing
                    f._CAE = Nothing
                    f._CAE_vto = Nothing
                    f._Mensaje = mensaje_obs_errores_eventos
                    MessageBox.Show("Se rechazó la factura. Detalle: " & vbCrLf & mensaje_obs_errores_eventos, "", MessageBoxButtons.OK, MessageBoxIcon.Error)

                Else
                    f._Estado = "Facturado"
                    f._CbeteDesde = CbteDesde
                    f._CbteHasta = CbteHasta
                    f._CAE = r.FeDetResp(0).CAE
                    f._CAE_vto = r.FeDetResp(0).CAEFchVto
                    f._Mensaje = mensaje_obs_errores_eventos
                    MessageBox.Show("Facturado exitosamente. Detalle: " & vbCrLf & mensaje_obs_errores_eventos, "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)

                    ' Imprimir tickets

                    If cbTicket.Checked = True Then
                        PrintDocumentFactura.PrinterSettings.PrinterName = My.Settings.Printer
                        PrintDocumentFactura.Print()
                    End If

                    If cbTicketCambio.Checked = True Then
                        PrintDocumentTicketCambio.PrinterSettings.PrinterName = My.Settings.Printer
                        PrintDocumentTicketCambio.Print()
                    End If

                End If
            End If

            'Crear factura
            idfactura = f.InsertarFactura

            'Crear detalles facturas
            For Each row As DataGridViewRow In dgvResumen.Rows
                Dim detalle As New DatosFacturas
                detalle._IdProducto = row.Cells("IdProducto").Value
                detalle._Descripcion = row.Cells("Descripcion").Value
                detalle._Marca = row.Cells("Marca").Value
                detalle._Color = row.Cells("Color").Value
                detalle._Talle = row.Cells("Talle").Value
                detalle._Cantidad = row.Cells("Cantidad").Value
                detalle._PrecioUnitario = row.Cells("PrecioUnitario").Value
                detalle._Subtotal = row.Cells("Subtotal").Value
                detalle._IdFactura = idfactura
                detalle.InsertarDetalleFactura()
            Next

            Me.DialogResult = DialogResult.OK
            Me.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocumentFactura.PrintPage

        If cboxComprobante.SelectedIndex = 0 Then
#Region "Ticket Común"

            Dim graphic As Graphics = e.Graphics

            Dim sf As New StringFormat
            sf.Alignment = StringAlignment.Center

            Dim font As New Font("Ticketing", 12)
            Dim fontbold As New Font("Ticketing", 16)
            Dim fontheight As Double = font.GetHeight
            Dim startx As Integer = 10
            Dim starty As Integer = 0
            Dim offset As Integer = 40
            Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

            ' Encabezado
            graphic.DrawString("LE PARC", font, New SolidBrush(Color.Black), 120, starty + offset)
            offset = offset + CInt(fontheight) + 10
            graphic.DrawString("Catamarca 314 - Tel: 3854996195", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Fecha: " & dtpFecha.Value.ToString("dd/MM/yyyy HH:mm"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Nro. Venta: " & idventa, font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3

            ' Detalles
            For Each row As DataGridViewRow In dgvResumen.Rows

                Dim desc As String = row.Cells("Descripcion").Value
                desc = desc.Substring(0, Math.Min(desc.Length, 18))
                Dim cant As String = row.Cells("Cantidad").Value
                Dim precio As Double = row.Cells("PrecioUnitario").Value
                Dim subt As Double = row.Cells("Cantidad").Value * row.Cells("PrecioUnitario").Value
                Dim formattedSubt As String = String.Format("{0:n}", subt)
                Dim linea1 As String = cant & "u x $" & precio
                Dim linea2 As String = desc
                Dim linea2total As String = " $" & formattedSubt

                graphic.DrawString(linea1, font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 2
                graphic.DrawString(linea2, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                graphic.DrawString(linea2total, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

                offset = offset + CInt(fontheight) + 3
            Next

            If descuento <> 0 Then

                offset = offset + CInt(fontheight) + 2
                graphic.DrawString("DESCUENTO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                graphic.DrawString("$" & descuento, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
                offset = offset + 3

            End If

            offset = offset + 20

            Dim rect As RectangleF = New RectangleF(startx, starty + offset, 265, 25)

            graphic.DrawString("TOTAL", fontbold, New SolidBrush(Color.Black), rect)
            graphic.DrawString("$" & total, fontbold, New SolidBrush(Color.Black), rect, format)

            offset = offset + CInt(fontheight) + 20

            graphic.DrawString("Comprobante no válido como factura", font, New SolidBrush(Color.Black), 15, starty + offset)
            offset = offset + 20
            graphic.DrawString("¡Gracias por su compra!", font, New SolidBrush(Color.Black), 63, starty + offset)
            offset = offset + CInt(fontheight) + 20
            graphic.DrawString("", font, New SolidBrush(Color.Black), 63, starty + offset)

            e.HasMorePages = False
#End Region
        ElseIf cboxComprobante.SelectedIndex = 1 Then
#Region "Factura A"

            Dim graphic As Graphics = e.Graphics

            Dim sf As New StringFormat
            sf.Alignment = StringAlignment.Center

            Dim font As New Font("Ticketing", 12)
            Dim fontbold As New Font("Ticketing", 16)
            Dim fontheight As Double = font.GetHeight
            Dim startx As Integer = 10
            Dim starty As Integer = 0
            Dim offset As Integer = 40
            Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

            ' Encabezado
            graphic.DrawString("PAEZ LAPRIDA MARCELO AGUSTIN", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("CUIT Nro.: 20-37411052-8", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("JUAN DELIBANO CHAZARRETA 114", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("SANTIAGO DEL ESTERO - CP (4200)", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("ING. BRUTOS: 20-37411052-8", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("INICIO DE ACTIVIDADES: 08/16", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IVA RESPONSABLE INSCRIPTO", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("TIQUE FACTURA A", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("P.V. Nro.: " & ptovta.ToString("0000"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Nro. T.: " & CbteDesde.ToString("00000000"), font, New SolidBrush(Color.Black), startx, starty + offset) 'nro ticket
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Fecha: " & dtpFecha.Value.ToString("dd/MM/yyyy HH:mm"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(nombrecliente, font, New SolidBrush(Color.Black), startx, starty + offset) 'nombre cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(tipopago, font, New SolidBrush(Color.Black), startx, starty + offset) 'pago
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(txtDocumento.Text, font, New SolidBrush(Color.Black), startx, starty + offset) 'cuit cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(direccioncliente, font, New SolidBrush(Color.Black), startx, starty + offset) 'direccion cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IVA RESPONSABLE INSCRIPTO", font, New SolidBrush(Color.Black), startx, starty + offset) 'tipo cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Cant./Precio Unit.", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Descripcion (%IVA)", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("IMPORTE", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3

            ' Detalles
            For Each row As DataGridViewRow In dgvResumen.Rows

                Dim desc As String = row.Cells("Descripcion").Value
                desc = desc.Substring(0, Math.Min(desc.Length, 18))
                Dim cant As String = row.Cells("Cantidad").Value
                Dim precio As Double = row.Cells("PrecioUnitario").Value
                Dim subt As Double = row.Cells("Cantidad").Value * row.Cells("PrecioUnitario").Value
                Dim formattedSubt As String = String.Format("{0:n}", subt)
                Dim linea1 As String = cant & "u x $" & precio
                Dim linea2 As String = desc & "(21)"
                Dim linea2total As String = " $" & formattedSubt

                graphic.DrawString(linea1, font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 2
                graphic.DrawString(linea2, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                graphic.DrawString(linea2total, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
                offset = offset + CInt(fontheight) + 3

            Next

            offset = offset + CInt(fontheight) + 3

            graphic.DrawString("IMP. NETO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & importeneto, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IMP. IVA", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & importeiva, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

            If descuento <> 0 Then
                offset = offset + CInt(fontheight) + 3
                graphic.DrawString("DESCUENTO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                graphic.DrawString("$" & descuento, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

            End If

            offset = offset + 20

            Dim rect As RectangleF = New RectangleF(startx, starty + offset, 265, 25)

            graphic.DrawString("TOTAL", fontbold, New SolidBrush(Color.Black), rect)
            graphic.DrawString("$" & importetotal, fontbold, New SolidBrush(Color.Black), rect, format)

            offset = offset + CInt(fontheight) + 20

            graphic.DrawString("RECIBI(MOS)", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("SU PAGO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & txtPago.Text, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("SU VUELTO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & txtVuelto.Text, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3

            'defino parametros del picturebox
            pb_codQR.Width = 175
            pb_codQR.Height = 175
            pb_codQR.BackgroundImageLayout = ImageLayout.Stretch

            Dim bm As New Bitmap(pb_codQR.Width, pb_codQR.Height)

            pb_codQR.DrawToBitmap(bm, New Rectangle(0, 0, pb_codQR.Width, pb_codQR.Height))

            e.Graphics.DrawImage(bm, startx + 40, starty + offset)

            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("", font, New SolidBrush(Color.Black), 63, starty + offset)

            e.HasMorePages = False
#End Region
        ElseIf cboxComprobante.SelectedIndex = 2 Then
#Region "Factura B"

            Dim graphic As Graphics = e.Graphics

            Dim sf As New StringFormat
            sf.Alignment = StringAlignment.Center

            Dim font As New Font("Ticketing", 12)
            Dim fontbold As New Font("Ticketing", 16)
            Dim fontheight As Double = font.GetHeight
            Dim startx As Integer = 10
            Dim starty As Integer = 0
            Dim offset As Integer = 40
            Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

            ' Encabezado
            graphic.DrawString("PAEZ LAPRIDA MARCELO AGUSTIN", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("CUIT Nro.: 20-37411052-8", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("JUAN DELIBANO CHAZARRETA 114", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("SANTIAGO DEL ESTERO - CP (4200)", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("ING. BRUTOS: 20-37411052-8", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("INICIO DE ACTIVIDADES: 08/16", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("IVA RESPONSABLE INSCRIPTO", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("TIQUE FACTURA B", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("P.V. Nro.: " & ptovta.ToString("0000"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Nro. T.: " & CbteDesde.ToString("000000000"), font, New SolidBrush(Color.Black), startx, starty + offset) 'nro ticket
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Fecha: " & dtpFecha.Value.ToString("dd/MM/yyyy HH:mm"), font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString(tipopago, font, New SolidBrush(Color.Black), startx, starty + offset) 'pago
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("DNI Nro.: " & txtDocumento.Text, font, New SolidBrush(Color.Black), startx, starty + offset) 'documento cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("A CONSUMIDOR FINAL", font, New SolidBrush(Color.Black), startx, starty + offset) 'tipo cliente
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Cant./Precio Unit.", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("Descripcion (%IVA)", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("IMPORTE", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3

            ' Detalles
            For Each row As DataGridViewRow In dgvResumen.Rows

                Dim desc As String = row.Cells("Descripcion").Value
                desc = desc.Substring(0, Math.Min(desc.Length, 18))
                Dim cant As String = row.Cells("Cantidad").Value
                Dim precio As Double = row.Cells("PrecioUnitario").Value
                Dim subt As Double = row.Cells("Cantidad").Value * row.Cells("PrecioUnitario").Value
                Dim formattedSubt As String = String.Format("{0:n}", subt)
                Dim linea1 As String = cant & "u x $" & precio
                Dim linea2 As String = desc & "(21)"
                Dim linea2total As String = " $" & formattedSubt

                graphic.DrawString(linea1, font, New SolidBrush(Color.Black), startx, starty + offset)
                offset = offset + CInt(fontheight) + 2
                graphic.DrawString(linea2, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                graphic.DrawString(linea2total, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
                offset = offset + CInt(fontheight) + 3

            Next

            offset = offset + CInt(fontheight) + 3

            graphic.DrawString("IMP. NETO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & importetotal, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

            If descuento <> 0 Then
                offset = offset + CInt(fontheight) + 3
                graphic.DrawString("DESCUENTO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
                graphic.DrawString("$" & descuento, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

            End If

            offset = offset + 20

            Dim rect As RectangleF = New RectangleF(startx, starty + offset, 265, 25)

            graphic.DrawString("TOTAL", fontbold, New SolidBrush(Color.Black), rect)
            graphic.DrawString("$" & importetotal, fontbold, New SolidBrush(Color.Black), rect, format)

            offset = offset + CInt(fontheight) + 20

            graphic.DrawString("RECIBI(MOS)", font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("SU PAGO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & txtPago.Text, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("SU VUELTO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & txtVuelto.Text, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + CInt(fontheight) + 3

            'defino parametros del picturebox
            pb_codQR.Width = 175
            pb_codQR.Height = 175
            pb_codQR.BackgroundImageLayout = ImageLayout.Stretch

            Dim bm As New Bitmap(pb_codQR.Width, pb_codQR.Height)

            pb_codQR.DrawToBitmap(bm, New Rectangle(0, 0, pb_codQR.Width, pb_codQR.Height))

            e.Graphics.DrawImage(bm, startx + 40, starty + offset)

            offset = offset + CInt(fontheight) + 3
            graphic.DrawString("", font, New SolidBrush(Color.Black), 63, starty + offset)

            e.HasMorePages = False
#End Region
        End If

    End Sub

    Private Sub txtPago_TextChanged(sender As Object, e As EventArgs) Handles txtPago.TextChanged
        If txtPago.Text = "" OrElse txtPago.Text = 0 Then
            txtVuelto.Text = 0
            txtPago.Text = 0
            txtPago.SelectAll()
        Else
            txtVuelto.Text = txtPago.Text - importetotal
        End If
    End Sub

    Private Sub txtPago_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPago.KeyPress
        'Verifica que solo se ingresen NUMEROS ENTEROS y la "coma"
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                If Asc(e.KeyChar) = 44 Then 'si quieres q sea "PUNTO" pone 46
                    Dim count As Integer = CountCharacter(txtPago.Text, e.KeyChar)
                    If count <> 0 Then
                        e.Handled = True
                    End If
                Else
                    e.Handled = True
                End If
            End If
        End If
    End Sub

    Private Sub txtPago_Click(sender As Object, e As EventArgs) Handles txtPago.Click
        txtPago.SelectAll()
    End Sub

    Private Sub txtDocumento_TextChanged(sender As Object, e As EventArgs) Handles txtDocumento.TextChanged

        If Trim(txtDocumento.Text) = "" Then
            txtDocumento.Text = "99999999"
        Else
            nombrecliente = "1"
            direccioncliente = "1"
            telefonocliente = "1"
        End If

    End Sub

    Private Sub bttElegirCliente_Click(sender As Object, e As EventArgs) Handles bttElegirCliente.Click

        Dim f As New ElegirCliente

        Try
            If f.ShowDialog() = DialogResult.OK Then

                txtDocumento.Text = f.DataGridView1.SelectedCells.Item(1).Value.ToString
                nombrecliente = f.DataGridView1.SelectedCells.Item(2).Value.ToString & " " & f.DataGridView1.SelectedCells.Item(3).Value.ToString
                direccioncliente = f.DataGridView1.SelectedCells.Item(4).Value.ToString
                telefonocliente = f.DataGridView1.SelectedCells.Item(5).Value.ToString

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try

    End Sub

    Private Sub Facturación_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub PrintDocumentTicketCambio_PrintPage(sender As Object, e As PrintPageEventArgs) Handles PrintDocumentTicketCambio.PrintPage

        Dim graphic As Graphics = e.Graphics

        Dim sf As New StringFormat
        sf.Alignment = StringAlignment.Center

        Dim font As New Font("Ticketing", 12)
        Dim fontbold As New Font("Ticketing", 16)
        Dim fontheight As Double = font.GetHeight
        Dim startx As Integer = 10
        Dim starty As Integer = 0
        Dim offset As Integer = 40
        Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

        ' Encabezado
        graphic.DrawString("LE PARC", font, New SolidBrush(Color.Black), 120, starty + offset)
        offset = offset + CInt(fontheight) + 10
        graphic.DrawString("Ticket de cambio", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("Catamarca 314 - Tel: 3854996195", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("Fecha: " & dtpFecha.Value.ToString("dd/MM/yyyy HH:mm"), font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("Nro. Venta: " & idventa, font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("Cantidad - Descripción", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3

        ' Detalles
        For Each row As DataGridViewRow In dgvResumen.Rows

            Dim desc As String = row.Cells("Descripcion").Value
            desc = desc.Substring(0, Math.Min(desc.Length, 30))
            Dim cant As String = row.Cells("Cantidad").Value
            Dim linea As String = cant & "u - " & desc

            graphic.DrawString(linea, font, New SolidBrush(Color.Black), startx, starty + offset)

            offset = offset + CInt(fontheight) + 3
        Next

        offset = offset + CInt(fontheight) + 10

        graphic.DrawString("Comprobante no válido como factura", font, New SolidBrush(Color.Black), 15, starty + offset)
        offset = offset + 20
        graphic.DrawString("¡Gracias por su compra!", font, New SolidBrush(Color.Black), 63, starty + offset)
        offset = offset + CInt(fontheight) + 20
        graphic.DrawString("", font, New SolidBrush(Color.Black), 63, starty + offset)

        e.HasMorePages = False

    End Sub

    Private Sub dtpFecha_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecha.ValueChanged
        cbtefch = dtpFecha.Value.ToString("yyyyMMdd")
    End Sub
End Class