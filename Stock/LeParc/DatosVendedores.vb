﻿Imports System.Data.SqlClient


Public Class DatosVendedores
    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

#Region "GetSet"

    Private IdVendedor As Int64
    Public Property IdVendedor_() As Int64
        Get
            Return IdVendedor
        End Get
        Set(ByVal value As Int64)
            IdVendedor = value
        End Set
    End Property

    Private Nombre As String
    Public Property Nombre_() As String
        Get
            Return Nombre
        End Get
        Set(ByVal value As String)
            Nombre = value
        End Set
    End Property

#End Region


    Public Sub InsertarVendedor()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Comando.Parameters.AddWithValue("@Nombre", Nombre)
        Comando.CommandText = "INSERT INTO Vendedores(Nombre) " +
                               "VALUES(@Nombre)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ModificarVendedor()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@IdVendedor", IdVendedor)
        Comando.Parameters.AddWithValue("@Nombre", Nombre)

        Comando.CommandText = "UPDATE Vendedores SET Nombre = @Nombre WHERE IdVendedor = @IdVendedor"

        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub EliminarVendedor()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdVendedor", IdVendedor)
        Comando.CommandText = "DELETE from Vendedores WHERE IdVendedor = @IdVendedor"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Function ObtenerTodo() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("SELECT * from Vendedores ORDER BY Nombre ASC", Cadena)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function
End Class
