﻿Public Class Cambio

    ' para el constructor
    Dim fila As New DataGridViewRow

    Dim idventa As Int64
    Dim cantidad As Integer
    Dim montonuevo As Double
    Dim montoanterior As Double
    Dim totalapagar As Double

    Sub New(_fila As DataGridViewRow, _cantidad As Integer)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        fila = _fila
        cantidad = _cantidad

    End Sub

    Private Sub CrearColumnasAgregados()

        dgvResumen.ColumnCount = 9
        dgvResumen.Columns(0).Name = "IdProducto"
        dgvResumen.Columns(1).Name = "Producto"
        dgvResumen.Columns(2).Name = "Marca"
        dgvResumen.Columns(3).Name = "Color"
        dgvResumen.Columns(4).Name = "Talle"
        dgvResumen.Columns(5).Name = "Cant."
        dgvResumen.Columns(6).Name = "Precio Min."
        dgvResumen.Columns(7).Name = "Precio May."
        dgvResumen.Columns(8).Name = "Precio Tarj."

        dgvResumen.Columns("Producto").FillWeight = 50
        dgvResumen.Columns("Talle").FillWeight = 15
        dgvResumen.Columns("Cant.").FillWeight = 15
        dgvResumen.Columns("Precio Min.").FillWeight = 20

        dgvResumen.Columns("IdProducto").Visible = False
        dgvResumen.Columns("Marca").Visible = False
        dgvResumen.Columns("Color").Visible = False
        dgvResumen.Columns("Precio May.").Visible = False
        dgvResumen.Columns("Precio Tarj.").Visible = False

    End Sub

    Private Sub txtBuscar_Click(sender As Object, e As EventArgs) Handles txtBuscar.Click
        If txtBuscar.Text = "Presione ENTER para buscar" Then
            txtBuscar.Text = ""
        Else
            txtBuscar.SelectAll()
        End If
    End Sub

    Private Sub txtBuscar_KeyDown(sender As Object, e As KeyEventArgs) Handles txtBuscar.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim indu As New DatosProductos

            'buscar por el combobox

            Try

                If (txtBuscar.Text <> "") Then
                    Dim dt As New DataTable
                    Dim Bandera As New Integer

                    If cboxBuscar.SelectedIndex = 0 Then 'codigo
                        indu.Codigo_ = txtBuscar.Text
                        Bandera = 0

                    ElseIf cboxBuscar.SelectedIndex = 1 Then 'talle
                        indu.Talle_ = txtBuscar.Text
                        Bandera = 1

                    ElseIf cboxBuscar.SelectedIndex = 2 Then 'marca
                        indu.Marca_ = txtBuscar.Text
                        Bandera = 2
                    ElseIf cboxBuscar.SelectedIndex = 3 Then 'color
                        indu.colorname_ = txtBuscar.Text
                        Bandera = 3
                    ElseIf cboxBuscar.SelectedIndex = 4 Then 'descripcion
                        indu.Descripcion_ = txtBuscar.Text
                        Bandera = 4
                    ElseIf cboxBuscar.SelectedIndex = 5 Then 'todos
                        If (txtBuscar.Text.Contains(" ")) Then
                            indu.Descripcion_ = txtBuscar.Text.Split(" ")(0).Replace(" ", "")
                            indu.Marca_ = txtBuscar.Text.Split(" ")(1).Replace(" ", "")
                        Else
                            indu.Descripcion_ = txtBuscar.Text
                        End If

                        Bandera = 5
                    End If


                    dgvProductos.DataSource = indu.Buscar(dt, Bandera)
                Else
                    dgvProductos.DataSource = indu.ObtenerTodo()
                End If

                dgvProductos.Columns("IdProducto").Visible = False
                dgvProductos.Columns("Color").Visible = False
                dgvProductos.Columns("Activo").Visible = False
                dgvProductos.Columns("IdUsuario_A").Visible = False
                dgvProductos.Columns("IdUsuario_M").Visible = False
                dgvProductos.Columns("Fecha_A").Visible = False
                dgvProductos.Columns("Fecha_M").Visible = False

                If dgvProductos.RowCount <> 0 Then
                    For Each row As DataGridViewRow In dgvProductos.Rows
                        row.Cells("NombreColor").Style.BackColor = Color.FromArgb(Convert.ToInt32(row.Cells("Color").Value))
                    Next
                End If

                dgvProductos.Columns("Codigo").FillWeight = 5
                dgvProductos.Columns("Descripcion").FillWeight = 21
                dgvProductos.Columns("Marca").FillWeight = 10
                dgvProductos.Columns("Cantidad").FillWeight = 10
                dgvProductos.Columns("Talle").FillWeight = 8
                dgvProductos.Columns("NombreColor").FillWeight = 10
                dgvProductos.Columns("PrecioMinorista").FillWeight = 12
                dgvProductos.Columns("PrecioMayorista").FillWeight = 12
                dgvProductos.Columns("PrecioTarjeta").FillWeight = 12

                dgvProductos.Columns("NombreColor").HeaderText = "Color"
                dgvProductos.Columns("PrecioMinorista").HeaderText = "$ Min."
                dgvProductos.Columns("PrecioMayorista").HeaderText = "$ May."
                dgvProductos.Columns("PrecioTarjeta").HeaderText = "$ Tarj."
                dgvProductos.Columns("Cantidad").HeaderText = "Cant."
                dgvProductos.ClearSelection()
            Catch ex As Exception
                MessageBox.Show("Ocurrio un error al realizar la busqueda. Detalles: " & ex.Message.ToString)
            End Try
        End If
    End Sub

    Private Sub txtBuscar_Leave(sender As Object, e As EventArgs) Handles txtBuscar.Leave
        If Trim(txtBuscar.Text) = "" Then
            txtBuscar.Text = "Presione ENTER para buscar"
        End If
    End Sub

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click

        Dim b As Integer = 0
        If dgvResumen.Rows.Count <> 0 Then

            LabelNoStock.Visible = False

#Region "for each para buscar si existe el producto"
            For Each row As DataGridViewRow In dgvResumen.Rows
                If dgvProductos.SelectedRows(0).Cells("IdProducto").Value = row.Cells("IdProducto").Value Then '' Si ya está el producto en la tabla agregados

                    If Trim(row.Cells("Cant.").Value) >= Trim(dgvProductos.SelectedRows(0).Cells("Cantidad").Value) Then
                        LabelNoStock.Visible = True
                        Return
                    Else
                        row.Cells("Cant.").Value = row.Cells("Cant.").Value + 1
                        LabelNoStock.Visible = False
                        b = 1
                        Exit For
                    End If

                End If
            Next
#End Region

#Region "en el caso de que el producto no esta agregado(sea uno nuevo)"
            If b = 0 Then
                dgvResumen.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("IdProducto").Value,
                                              dgvProductos.CurrentRow.Cells("Descripcion").Value,
                                              dgvProductos.CurrentRow.Cells("Marca").Value,
                                              dgvProductos.CurrentRow.Cells("NombreColor").Value,
                                             dgvProductos.CurrentRow.Cells("Talle").Value,
                                             1,
                                             dgvProductos.CurrentRow.Cells("PrecioMinorista").Value,
                                             dgvProductos.CurrentRow.Cells("PrecioMayorista").Value,
                                             dgvProductos.CurrentRow.Cells("PrecioTarjeta").Value})

            End If
#End Region

        Else '' Cuando la tabla agregados está vacía. Pasa una sola vez

            CrearColumnasAgregados()

            dgvResumen.Rows.Add(New String() {dgvProductos.CurrentRow.Cells("IdProducto").Value,
                                              dgvProductos.CurrentRow.Cells("Descripcion").Value,
                                              dgvProductos.CurrentRow.Cells("Marca").Value,
                                              dgvProductos.CurrentRow.Cells("NombreColor").Value,
                                             dgvProductos.CurrentRow.Cells("Talle").Value,
                                             1,
                                    dgvProductos.CurrentRow.Cells("PrecioMinorista").Value,
                                    dgvProductos.CurrentRow.Cells("PrecioMayorista").Value,
                                    dgvProductos.CurrentRow.Cells("PrecioTarjeta").Value})

        End If

        ' Actualizar total
        If cboxPrecio.SelectedIndex = 0 Then 'minorista

            montonuevo = montonuevo + dgvProductos.CurrentRow.Cells("PrecioMinorista").Value 

        ElseIf cboxPrecio.SelectedIndex = 1 Then 'tarjeta

            montonuevo = montonuevo + dgvProductos.CurrentRow.Cells("PrecioTarjeta").Value

        ElseIf cboxPrecio.SelectedIndex = 2 Then 'mayorista

            montonuevo = montonuevo + dgvProductos.CurrentRow.Cells("PrecioMayorista").Value

        End If

        totalapagar = montonuevo - montoanterior

        lblMontoNuevo.Text = "Monto nuevo: $ " & montonuevo
        lblPagar.Text = "A pagar: $ " & totalapagar

    End Sub

    Private Sub Cambio_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        cboxBuscar.SelectedIndex = 0
        cboxPrecio.SelectedIndex = 0
        txtBuscar.Focus()

        idventa = fila.Cells("IdVenta").Value

        montoanterior = fila.Cells("PrecioUnitario").Value * cantidad

        lblMontoFavor.Text = "Monto a favor: $ " & montoanterior
        lblProducto.Text = fila.Cells("Descripcion").Value & ", " & fila.Cells("Marca").Value & ", " & fila.Cells("Talle").Value & " x" & cantidad

    End Sub

    Private Sub cboxPrecio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboxPrecio.SelectedIndexChanged

        If dgvResumen.RowCount <> 0 Then

            montonuevo = 0

            For Each row As DataGridViewRow In dgvResumen.Rows

                If cboxPrecio.SelectedIndex = 0 Then 'minorista

                    montonuevo = montonuevo + row.Cells("Precio Min.").Value * dgvResumen.SelectedRows(0).Cells("Cant.").Value

                ElseIf cboxPrecio.SelectedIndex = 1 Then 'tarjeta

                    montonuevo = montonuevo + row.Cells("Precio Tarj.").Value * dgvResumen.SelectedRows(0).Cells("Cant.").Value

                ElseIf cboxPrecio.SelectedIndex = 2 Then 'mayorista

                    montonuevo = montonuevo + row.Cells("Precio May.").Value * dgvResumen.SelectedRows(0).Cells("Cant.").Value

                End If

            Next

            totalapagar = montonuevo - montoanterior

            lblMontoNuevo.Text = "Monto nuevo: $ " & montonuevo
            lblPagar.Text = "A pagar: $ " & totalapagar

        End If

    End Sub

    Private Sub dgvProductos_SelectionChanged(sender As Object, e As EventArgs) Handles dgvProductos.SelectionChanged

        LabelNoStock.Visible = False

        If dgvProductos.SelectedRows.Count <> 0 Then
            If dgvProductos.SelectedRows(0).Cells("Cantidad").Value > 0 Then
                bttAgregar.Enabled = True
            Else
                bttAgregar.Enabled = False
            End If
        Else
            bttAgregar.Enabled = False
        End If

    End Sub

    Private Sub bttCancelar_Click(sender As Object, e As EventArgs) Handles bttCancelar.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub bttAceptar_Click(sender As Object, e As EventArgs) Handles bttAceptar.Click

        Try

            If dgvResumen.RowCount <> 0 Then

                Dim detalle As New DatosVentas

                ' Eliminar detalle de venta anterior
                If fila.Cells("Cantidad").Value = cantidad Then ' Cambia todo, elimina el detalle

                    detalle.IdDetalle_ = fila.Cells("IdDetalle").Value
                    detalle.ElimiarDetalle()

                Else ' cambia x, actualiza la cantidad del detalle y 

                    detalle.IdDetalle_ = fila.Cells("IdDetalle").Value
                    detalle.Cantidad_ = cantidad
                    detalle.Subtotal_ = fila.Cells("PrecioUnitario").Value * cantidad
                    detalle.ActualizarCantidadDetalle()

                End If

                ' Aumentar stock de producto anterior
                Dim aum As New DatosProductos
                aum.IdProducto_ = fila.Cells("IdProducto").Value
                aum.Cantidad_ = cantidad
                aum.AumentarProducto()

                ' Crear nuevo(s) detalle(s) de venta
                For Each row As DataGridViewRow In dgvResumen.Rows

                    Dim d As New DatosVentas
                    Dim p As New DatosProductos

                    d.IdProducto_ = row.Cells("IdProducto").Value
                    d.Descripcion_ = row.Cells("Producto").Value
                    d.Marca_ = row.Cells("Marca").Value
                    d.Color_ = row.Cells("Color").Value
                    d.Talle_ = row.Cells("Talle").Value
                    d.Cantidad_ = row.Cells("Cant.").Value

                    If cboxPrecio.SelectedIndex = 0 Then 'minorista

                        d.PrecioUnitario_ = row.Cells("Precio Min.").Value
                        d.Subtotal_ = row.Cells("Precio Min.").Value * row.Cells("Cant.").Value

                    ElseIf cboxPrecio.SelectedIndex = 1 Then 'tarjeta

                        d.PrecioUnitario_ = row.Cells("Precio Tarj.").Value
                        d.Subtotal_ = row.Cells("Precio Tarj.").Value * row.Cells("Cant.").Value

                    ElseIf cboxPrecio.SelectedIndex = 2 Then 'mayorista

                        d.PrecioUnitario_ = row.Cells("Precio May.").Value
                        d.Subtotal_ = row.Cells("Precio May.").Value * row.Cells("Cant.").Value

                    End If

                    d.IdVentas_ = idventa
                    d.CrearDetalleVenta()

                    ' Disminuir stock de producto
                    p.IdProducto_ = row.Cells("IdProducto").Value
                    p.Cantidad_ = row.Cells("Cant.").Value
                    p.DisminuirProducto()

                Next

                ' Actualizar total de venta
                Dim venta As New DatosVentas
                venta.IdVentas_ = idventa

                If totalapagar > 0 Then
                    venta.Total_ = fila.Cells("Total").Value + totalapagar
                Else
                    venta.Total_ = fila.Cells("Total").Value
                End If

                venta.ActualizarTotalVenta()

                MessageBox.Show("Cambio realizado exitosamente.")
                Me.DialogResult = DialogResult.OK
                Me.Close()

            Else
                MessageBox.Show("Debe elegir al menos un producto para registrar el cambio.")
            End If

        Catch ex As Exception
            MsgBox("Ocurrió un error al realizar el cambio. Detalle: ", ex.Message.ToString)
        End Try

    End Sub

    Private Sub dgvResumen_KeyDown(sender As Object, e As KeyEventArgs) Handles dgvResumen.KeyDown

        If e.KeyCode = Keys.Delete Then
            If dgvResumen.SelectedRows.Count <> 0 Then

                If cboxPrecio.SelectedIndex = 0 Then 'minorista

                    montonuevo = montonuevo - dgvResumen.SelectedRows(0).Cells("Precio Min.").Value * dgvResumen.SelectedRows(0).Cells("Cant.").Value

                ElseIf cboxPrecio.SelectedIndex = 1 Then 'tarjeta

                    montonuevo = montonuevo - dgvResumen.SelectedRows(0).Cells("Precio Min.").Value * dgvResumen.SelectedRows(0).Cells("Cant.").Value

                ElseIf cboxPrecio.SelectedIndex = 2 Then 'mayorista

                    montonuevo = montonuevo - dgvResumen.SelectedRows(0).Cells("Precio Min.").Value * dgvResumen.SelectedRows(0).Cells("Cant.").Value

                End If

                totalapagar = montonuevo - montoanterior

                lblMontoNuevo.Text = "Monto nuevo: $ " & montonuevo
                lblPagar.Text = "A pagar: $ " & totalapagar

                dgvResumen.Rows.Remove(dgvResumen.SelectedRows(0))

            End If
        End If

    End Sub

End Class