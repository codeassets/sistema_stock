﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Config
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Config))
        Me.Label10 = New System.Windows.Forms.Label()
        Me.PanelColor = New System.Windows.Forms.Panel()
        Me.PanelALL = New System.Windows.Forms.Panel()
        Me.ImageListIconos = New System.Windows.Forms.ImageList(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.bttTalles = New System.Windows.Forms.Button()
        Me.bttMarcas = New System.Windows.Forms.Button()
        Me.bttVendedores = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label10
        '
        Me.Label10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Averta Demo PE Cutted Demo", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(25, 20)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(215, 38)
        Me.Label10.TabIndex = 36
        Me.Label10.Text = "Configuración"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PanelColor
        '
        Me.PanelColor.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(74, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.PanelColor.Location = New System.Drawing.Point(66, 123)
        Me.PanelColor.Name = "PanelColor"
        Me.PanelColor.Size = New System.Drawing.Size(45, 5)
        Me.PanelColor.TabIndex = 43
        '
        'PanelALL
        '
        Me.PanelALL.Location = New System.Drawing.Point(66, 135)
        Me.PanelALL.Name = "PanelALL"
        Me.PanelALL.Size = New System.Drawing.Size(758, 336)
        Me.PanelALL.TabIndex = 65
        '
        'ImageListIconos
        '
        Me.ImageListIconos.ImageStream = CType(resources.GetObject("ImageListIconos.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListIconos.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageListIconos.Images.SetKeyName(0, "talles.png")
        Me.ImageListIconos.Images.SetKeyName(1, "impresora.png")
        Me.ImageListIconos.Images.SetKeyName(2, "marcas.png")
        Me.ImageListIconos.Images.SetKeyName(3, "user.png")
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.BackColor = System.Drawing.Color.White
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.ImageIndex = 1
        Me.Button1.ImageList = Me.ImageListIconos
        Me.Button1.Location = New System.Drawing.Point(253, 88)
        Me.Button1.Margin = New System.Windows.Forms.Padding(0)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(124, 35)
        Me.Button1.TabIndex = 67
        Me.Button1.Text = "Impresora"
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.Button1.UseVisualStyleBackColor = False
        '
        'bttTalles
        '
        Me.bttTalles.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.bttTalles.BackColor = System.Drawing.Color.White
        Me.bttTalles.FlatAppearance.BorderSize = 0
        Me.bttTalles.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttTalles.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttTalles.ForeColor = System.Drawing.Color.Black
        Me.bttTalles.ImageIndex = 0
        Me.bttTalles.ImageList = Me.ImageListIconos
        Me.bttTalles.Location = New System.Drawing.Point(164, 88)
        Me.bttTalles.Margin = New System.Windows.Forms.Padding(0)
        Me.bttTalles.Name = "bttTalles"
        Me.bttTalles.Size = New System.Drawing.Size(89, 35)
        Me.bttTalles.TabIndex = 66
        Me.bttTalles.Text = "Talles"
        Me.bttTalles.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttTalles.UseVisualStyleBackColor = False
        '
        'bttMarcas
        '
        Me.bttMarcas.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.bttMarcas.BackColor = System.Drawing.Color.White
        Me.bttMarcas.FlatAppearance.BorderSize = 0
        Me.bttMarcas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttMarcas.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttMarcas.ForeColor = System.Drawing.Color.Black
        Me.bttMarcas.ImageIndex = 2
        Me.bttMarcas.ImageList = Me.ImageListIconos
        Me.bttMarcas.Location = New System.Drawing.Point(66, 88)
        Me.bttMarcas.Margin = New System.Windows.Forms.Padding(0)
        Me.bttMarcas.Name = "bttMarcas"
        Me.bttMarcas.Size = New System.Drawing.Size(98, 35)
        Me.bttMarcas.TabIndex = 42
        Me.bttMarcas.Text = "Marcas"
        Me.bttMarcas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttMarcas.UseVisualStyleBackColor = False
        '
        'bttVendedores
        '
        Me.bttVendedores.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.bttVendedores.BackColor = System.Drawing.Color.White
        Me.bttVendedores.FlatAppearance.BorderSize = 0
        Me.bttVendedores.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bttVendedores.Font = New System.Drawing.Font("Averta Regular", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bttVendedores.ForeColor = System.Drawing.Color.Black
        Me.bttVendedores.ImageIndex = 3
        Me.bttVendedores.ImageList = Me.ImageListIconos
        Me.bttVendedores.Location = New System.Drawing.Point(377, 88)
        Me.bttVendedores.Margin = New System.Windows.Forms.Padding(0)
        Me.bttVendedores.Name = "bttVendedores"
        Me.bttVendedores.Size = New System.Drawing.Size(146, 35)
        Me.bttVendedores.TabIndex = 68
        Me.bttVendedores.Text = "Vendedores"
        Me.bttVendedores.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.bttVendedores.UseVisualStyleBackColor = False
        '
        'Config
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(891, 523)
        Me.Controls.Add(Me.bttVendedores)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.bttTalles)
        Me.Controls.Add(Me.PanelALL)
        Me.Controls.Add(Me.PanelColor)
        Me.Controls.Add(Me.bttMarcas)
        Me.Controls.Add(Me.Label10)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Config"
        Me.Resizable = False
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Style = MetroFramework.MetroColorStyle.White
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label10 As Label
    Friend WithEvents PanelColor As Panel
    Friend WithEvents bttMarcas As Button
    Friend WithEvents PanelALL As Panel
    Friend WithEvents ImageListIconos As ImageList
    Friend WithEvents bttTalles As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents bttVendedores As Button
End Class
