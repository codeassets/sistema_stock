﻿Public Class Ventas

    Dim datosventas As New DatosVentas
    Dim datosclientes As New DatosClientes

    Dim tipobusqueda As String

    Private Sub ColorearAnuladas()
        For Each row As DataGridViewRow In dgvVentas.Rows
            If row.Cells("Activo").Value = False Then
                row.DefaultCellStyle.BackColor = Color.Salmon
            End If
        Next
    End Sub


    Private Function ContrastColor(ByVal color As Color) As Color
        Dim d As Integer = 0
        Dim luminance As Double = (0.299 * color.R + 0.587 * color.G + 0.114 * color.B) / 255

        If luminance > 0.5 Then
            d = 0
        Else
            d = 255
        End If

        Return Color.FromArgb(d, d, d)
    End Function

    Private Sub Ventas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtpDesde.Value = Date.Today
        dtpHasta.Value = Date.Today
        txtAño.Text = Date.Today.Year
        cboxMes.SelectedIndex = Now.Month - 1
        dgvVentas.RowsDefaultCellStyle.BackColor = Color.WhiteSmoke
    End Sub

    Private Sub bttAceptar_Click(sender As Object, e As EventArgs) Handles bttBuscarMesAño.Click
        Try
            txtDocumento.Clear()

            dgvDetalles.DataSource = Nothing

            Dim mes As String = cboxMes.SelectedIndex + 1
            Dim año As String = txtAño.Text

            'castear la fecha completa
            'Dim fechadesde As String = mes & "/01/" & año
            'Dim fechahasta As String = mes & "/" & Date.DaysInMonth(Integer.Parse(año), Integer.Parse(mes)) & "/" & año

            dgvVentas.DataSource = datosventas.BuscarPorMesyAño(mes, año)
            dgvVentas.Columns("Activo").Visible = False
            dgvVentas.Columns("IdVenta").HeaderText = "Nro. Venta"
            ColorearAnuladas()

            Dim total As Double
            For Each row As DataGridViewRow In dgvVentas.Rows
                If row.Cells("Activo").Value = True Then
                    total = total + row.Cells("Total").Value
                End If
            Next

            lblTotal.Text = "Total: $" & total

            lblTitulo.Text = "Ventas de " & cboxMes.SelectedItem.ToString & " de " & txtAño.Text

            lblTituloDetalles.Text = "Detalles"

            tipobusqueda = "Meses"

        Catch ex As Exception
            MessageBox.Show("Ocurrió un problema al buscar la venta. Detalle: " & ex.Message.ToString)
        End Try

    End Sub

    Private Sub txtAño_TextChanged(sender As Object, e As EventArgs) Handles txtAño.TextChanged

        If txtAño.TextLength <> 4 Then
            bttBuscarMesAño.Enabled = False
        End If

        If txtAño.TextLength = 4 Then
            bttBuscarMesAño.Enabled = True
        End If

    End Sub

    Private Sub bttBuscar_Click(sender As Object, e As EventArgs) Handles bttBuscarCliente.Click

        Dim f As New ElegirCliente

        Try
            f.ShowDialog()

            ElegirCliente.b = 1

            dgvDetalles.DataSource = Nothing

            If f.DataGridView1.SelectedRows.Count <> 0 Then
                txtDocumento.Text = f.DataGridView1.SelectedCells.Item(1).Value.ToString

                If txtDocumento.Text <> "" Then
                    datosventas.Documento_ = txtDocumento.Text
                    dgvVentas.DataSource = datosventas.BuscarxCliente
                    dgvVentas.Columns("IdVenta").Visible = False
                    dgvVentas.Columns("Activo").Visible = False
                    ColorearAnuladas()



                    tipobusqueda = "Cliente"

                End If

                Dim total As Double
                For Each row As DataGridViewRow In dgvVentas.Rows
                    total = total + row.Cells("Total").Value
                Next
                lblTotal.Text = "Total: $" & total
            Else

                dgvVentas.DataSource = Nothing

            End If

            lblTitulo.Text = "Ventas del cliente " & txtDocumento.Text

            lblTituloDetalles.Text = "Detalles"

        Catch ex As Exception
            MessageBox.Show("Ocurrió un error al buscar las ventas del cliente Documento:" & txtDocumento.Text & ". Detalle: " & ex.Message.ToString)
        End Try

    End Sub

    Private Sub txtDNI_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDocumento.KeyPress
        If e.KeyChar <> ChrW(Keys.Back) Then
            If Char.IsNumber(e.KeyChar) Then
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtAño_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtAño.KeyPress
        If e.KeyChar <> ChrW(Keys.Back) Then
            If Char.IsNumber(e.KeyChar) Then
            Else
                e.Handled = True
            End If
        End If
    End Sub


    Private Sub DataGridViewVentas_SelectionChanged(sender As Object, e As EventArgs)
        If dgvVentas.SelectedRows.Count <> 0 Then
            dgvVentas.RowsDefaultCellStyle.SelectionBackColor = Color.FromArgb(Convert.ToInt32(dgvVentas.CurrentRow.Cells("Color").Value))
            Dim forecolor As Color = ContrastColor(Color.FromArgb(Convert.ToInt32(dgvVentas.CurrentRow.Cells("Color").Value)))
            dgvVentas.RowsDefaultCellStyle.SelectionForeColor = forecolor
        End If
    End Sub

    Private Sub btnBuscarPorDia_Click(sender As Object, e As EventArgs) Handles btnBuscarFechas.Click
        Try
            txtDocumento.Clear()
            dgvDetalles.DataSource = Nothing
            dgvVentas.DataSource = datosventas.BuscarVenta(dtpDesde.Value.ToShortDateString, dtpHasta.Value.ToShortDateString)
            dgvVentas.Columns("Activo").Visible = False
            dgvVentas.Columns("IdVenta").HeaderText = "Nro. Venta"
            ColorearAnuladas()

            Dim total As Double
            For Each row As DataGridViewRow In dgvVentas.Rows 'row=dgvVentas.selectedrows(0)
                If row.Cells("Activo").Value = True Then
                    total = total + row.Cells("Total").Value
                End If

            Next

            lblTotal.Text = "Total: $" & total

            lblTitulo.Text = "Ventas del día " & dtpDesde.Text & " al día " & dtpHasta.Text

            lblTituloDetalles.Text = "Detalles"

            tipobusqueda = "Fechas"

        Catch ex As Exception
            MessageBox.Show("Ocurrió un problema al buscar la venta. Detalle: " & ex.Message.ToString)
        End Try

    End Sub

    Private Sub dgvVentas_SelectionChanged(sender As Object, e As EventArgs) Handles dgvVentas.SelectionChanged
        If dgvVentas.SelectedRows.Count <> 0 Then
            dgvDetalles.DataSource = Nothing
            lblTituloDetalles.Text = "Detalles"
            bttAnular.Enabled = False
        End If
    End Sub

    Private Sub dgvDetalles_SelectionChanged(sender As Object, e As EventArgs) Handles dgvDetalles.SelectionChanged

        Try
            If dgvDetalles.SelectedRows.Count <> 0 Then

                If dgvVentas.SelectedRows(0).Cells("Activo").Value = True Then

                    bttCambio.Enabled = True
                    bttImprimirTicket.Enabled = True
                    bttImprimirTicketCambio.Enabled = True


                    dgvDetalles.RowsDefaultCellStyle.SelectionBackColor = Color.FromArgb(Convert.ToInt32(dgvDetalles.CurrentRow.Cells("CodigoColor").Value))
                    Dim forecolor As Color = ContrastColor(Color.FromArgb(Convert.ToInt32(dgvDetalles.CurrentRow.Cells("CodigoColor").Value)))
                    dgvDetalles.RowsDefaultCellStyle.SelectionForeColor = forecolor

                    If dgvDetalles.SelectedRows(0).Cells("Cantidad").Value > 1 Then
                        numCantidad.Enabled = True
                    Else
                        numCantidad.Enabled = False
                    End If

                    numCantidad.Maximum = dgvDetalles.SelectedRows(0).Cells("Cantidad").Value
                    numCantidad.Minimum = 1

                Else

                    bttCambio.Enabled = False
                    bttImprimirTicket.Enabled = False
                    bttImprimirTicketCambio.Enabled = False

                End If

            Else
                bttCambio.Enabled = False
                bttImprimirTicket.Enabled = False
                bttImprimirTicketCambio.Enabled = False
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub bttCambio_Click(sender As Object, e As EventArgs) Handles bttCambio.Click

        ' Abrir ventana de cambio
        Dim f As New Cambio(dgvDetalles.SelectedRows(0), numCantidad.Value)

        If f.ShowDialog() = DialogResult.OK Then
            ' Exito

            Dim id As Integer
            Dim detalle As New DatosVentas

            If dgvVentas.SelectedRows.Count <> 0 Then

                bttImprimirTicket.Enabled = True
                bttImprimirTicketCambio.Enabled = True

                Try
                    id = dgvVentas.SelectedRows(0).Cells("IdVenta").Value
                    detalle.IdVentas_ = id
                    dgvDetalles.DataSource = detalle.ObtenerDetalles()
                    dgvDetalles.Columns("Descripcion").HeaderText = "Producto"
                    dgvDetalles.Columns("PrecioUnitario").HeaderText = "Precio Unit."
                    dgvDetalles.Columns("IdProducto").Visible = False
                    dgvDetalles.Columns("CodigoColor").Visible = False
                    dgvDetalles.Columns("IdVenta").Visible = False
                    dgvDetalles.Columns("Total").Visible = False
                    dgvDetalles.Columns("IdDetalle").Visible = False
                Catch ex As Exception
                End Try

            End If

        Else
            ' No exito ahre

        End If

    End Sub

    Private Sub bttAnular_Click(sender As Object, e As EventArgs) Handles bttAnular.Click

        Dim result As DialogResult

        result = MessageBox.Show("¿Está seguro de que desea anular la Venta Nro.: " & dgvVentas.SelectedRows(0).Cells("IdVenta").Value & "?", "Confirmación", MessageBoxButtons.YesNo)

        Try
            If result = DialogResult.Yes Then

                Dim venta As New DatosVentas
                venta.IdVentas_ = dgvVentas.SelectedRows(0).Cells("IdVenta").Value
                venta.AnularVenta()

                ' Aumentar stock de productos
                For Each row As DataGridViewRow In dgvDetalles.Rows
                    Dim producto As New DatosProductos
                    producto.IdProducto_ = row.Cells("IdProducto").Value
                    producto.Cantidad_ = row.Cells("Cantidad").Value
                    producto.AumentarProducto()
                Next

                MessageBox.Show("Venta anulada.")

                If tipobusqueda = "Cliente" Then

                    datosventas.Documento_ = txtDocumento.Text
                    dgvVentas.DataSource = datosventas.BuscarxCliente

                ElseIf tipobusqueda = "Meses" Then

                    Dim mes As String = cboxMes.SelectedIndex + 1
                    Dim año As String = txtAño.Text

                    dgvVentas.DataSource = datosventas.BuscarPorMesyAño(mes, año)

                    lblTitulo.Text = "Ventas de " & cboxMes.SelectedItem.ToString & " de " & txtAño.Text

                ElseIf tipobusqueda = "Fechas" Then

                    dgvVentas.DataSource = datosventas.BuscarVenta(dtpDesde.Value.ToShortDateString, dtpHasta.Value.ToShortDateString)

                    lblTitulo.Text = "Ventas del día " & dtpDesde.Text & " al día " & dtpHasta.Text

                End If

                dgvVentas.Columns("IdVenta").Visible = False
                dgvVentas.Columns("Activo").Visible = False
                ColorearAnuladas()

                Dim total As Double
                For Each row As DataGridViewRow In dgvVentas.Rows
                    If row.Cells("Activo").Value = True Then
                        total = total + row.Cells("Total").Value
                    End If
                Next
                lblTotal.Text = "Total: $" & total

            End If
        Catch ex As Exception
            MessageBox.Show("Ocurrió un error al anular la venta. Detalle: " & ex.Message.ToString, "", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub txtDocumento_KeyDown(sender As Object, e As KeyEventArgs) Handles txtDocumento.KeyDown

        Try
            If e.KeyCode = Keys.Enter Then
                If txtDocumento.Text <> "" Then
                    datosventas.Documento_ = txtDocumento.Text
                    dgvVentas.DataSource = datosventas.BuscarxCliente
                    dgvVentas.Columns("IdVenta").Visible = False
                    dgvVentas.Columns("Activo").Visible = False
                    ColorearAnuladas()

                    Dim total As Double
                    For Each row As DataGridViewRow In dgvVentas.Rows
                        If row.Cells("Activo").Value = True Then
                            total = total + row.Cells("Total").Value
                        End If
                    Next
                    lblTotal.Text = "Total: $" & total

                    tipobusqueda = "Cliente"

                End If
            End If

        Catch ex As Exception
            MessageBox.Show("Ocurrió un error al buscar las ventas del cliente Documento:" & txtDocumento.Text & ". Detalle: " & ex.Message.ToString)
        End Try

    End Sub

    Private Sub dgvVentas_Sorted(sender As Object, e As EventArgs) Handles dgvVentas.Sorted

        If dgvVentas.RowCount <> 0 Then
            ColorearAnuladas()
        End If

    End Sub

    Private Sub bttImprimirTicket_Click(sender As Object, e As EventArgs) Handles bttImprimirTicket.Click
        PrintDocumentTicket.PrinterSettings.PrinterName = My.Settings.Printer
        PrintDocumentTicket.Print()
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocumentTicket.PrintPage

        Dim graphic As Graphics = e.Graphics

        Dim sf As New StringFormat
        sf.Alignment = StringAlignment.Center

        Dim font As New Font("Ticketing", 12)
        Dim fontbold As New Font("Ticketing", 16)
        Dim fontheight As Double = font.GetHeight
        Dim startx As Integer = 10
        Dim starty As Integer = 0
        Dim offset As Integer = 40
        Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

        ' Encabezado
        graphic.DrawString("LE PARC", font, New SolidBrush(Color.Black), 120, starty + offset)
        offset = offset + CInt(fontheight) + 10
        graphic.DrawString("Catamarca 314 - Tel: 3854996195", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("Fecha: " & dgvVentas.SelectedRows(0).Cells("Fecha").Value, font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("Nro. Venta: " & dgvVentas.SelectedRows(0).Cells("IdVenta").Value, font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3

        ' Detalles
        For Each row As DataGridViewRow In dgvDetalles.Rows

            Dim desc As String = row.Cells("Descripcion").Value
            desc = desc.Substring(0, Math.Min(desc.Length, 18))
            Dim cant As String = row.Cells("Cantidad").Value
            Dim precio As Double = row.Cells("PrecioUnitario").Value
            Dim subt As Double = row.Cells("Cantidad").Value * row.Cells("PrecioUnitario").Value
            Dim formattedSubt As String = String.Format("{0:n}", subt)
            Dim linea1 As String = cant & "u x $" & precio
            Dim linea2 As String = desc
            Dim linea2total As String = " $" & formattedSubt

            graphic.DrawString(linea1, font, New SolidBrush(Color.Black), startx, starty + offset)
            offset = offset + CInt(fontheight) + 2
            graphic.DrawString(linea2, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString(linea2total, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)

            offset = offset + CInt(fontheight) + 3
        Next

        If dgvVentas.SelectedRows(0).Cells("Descuento").Value <> 0 Then

            offset = offset + CInt(fontheight) + 2
            graphic.DrawString("DESCUENTO", font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25))
            graphic.DrawString("$" & dgvVentas.SelectedRows(0).Cells("Descuento").Value, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 265, 25), format)
            offset = offset + 3

        End If

        offset = offset + 20

        Dim rect As RectangleF = New RectangleF(startx, starty + offset, 265, 25)

        graphic.DrawString("TOTAL", fontbold, New SolidBrush(Color.Black), rect)
        graphic.DrawString("$" & dgvVentas.SelectedRows(0).Cells("Total").Value, fontbold, New SolidBrush(Color.Black), rect, format)

        offset = offset + CInt(fontheight) + 20

        graphic.DrawString("Comprobante no válido como factura", font, New SolidBrush(Color.Black), 15, starty + offset)
        offset = offset + 20
        graphic.DrawString("¡Gracias por su compra!", font, New SolidBrush(Color.Black), 63, starty + offset)
        offset = offset + CInt(fontheight) + 20
        graphic.DrawString("", font, New SolidBrush(Color.Black), 63, starty + offset)

        e.HasMorePages = False

    End Sub

    Private Sub dgvVentas_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvVentas.CellMouseDoubleClick

        Dim id As Integer
        Dim detalle As New DatosVentas

        Try
            If dgvVentas.SelectedRows.Count <> 0 Then

                bttImprimirTicket.Enabled = True
                bttImprimirTicketCambio.Enabled = True

                If dgvVentas.SelectedRows(0).Cells("Activo").Value = True Then
                    bttAnular.Enabled = True
                    bttCambio.Enabled = True
                Else
                    bttAnular.Enabled = False
                    bttCambio.Enabled = False
                End If

                id = dgvVentas.SelectedRows(0).Cells("IdVenta").Value
                detalle.IdVentas_ = id
                dgvDetalles.DataSource = detalle.ObtenerDetalles()
                dgvDetalles.Columns("Descripcion").HeaderText = "Producto"
                dgvDetalles.Columns("PrecioUnitario").HeaderText = "Precio Unit."
                dgvDetalles.Columns("IdProducto").Visible = False
                dgvDetalles.Columns("CodigoColor").Visible = False
                dgvDetalles.Columns("IdVenta").Visible = False
                dgvDetalles.Columns("Total").Visible = False
                dgvDetalles.Columns("IdDetalle").Visible = False
                dgvDetalles.Columns("Color").Visible = False

                lblTituloDetalles.Text = "Detalles de Venta Nro.: " & dgvVentas.SelectedRows(0).Cells("IdVenta").Value

            Else
                bttImprimirTicket.Enabled = False
                bttImprimirTicketCambio.Enabled = False
                bttAnular.Enabled = False
                bttCambio.Enabled = False
            End If

        Catch ex As Exception
        End Try

    End Sub

    Private Sub bttImprimirTicketCambio_Click(sender As Object, e As EventArgs) Handles bttImprimirTicketCambio.Click
        PrintDocumentTicketCambio.PrinterSettings.PrinterName = My.Settings.Printer
        PrintDocumentTicketCambio.Print()
    End Sub

    Private Sub PrintDocumentTicketCambio_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocumentTicketCambio.PrintPage

        Dim graphic As Graphics = e.Graphics

        Dim sf As New StringFormat
        sf.Alignment = StringAlignment.Center

        Dim font As New Font("Ticketing", 12)
        Dim fontbold As New Font("Ticketing", 16)
        Dim fontheight As Double = font.GetHeight
        Dim startx As Integer = 10
        Dim starty As Integer = 0
        Dim offset As Integer = 40
        Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

        ' Encabezado
        graphic.DrawString("LE PARC", font, New SolidBrush(Color.Black), 120, starty + offset)
        offset = offset + CInt(fontheight) + 10
        graphic.DrawString("Ticket de cambio", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("Catamarca 314 - Tel: 3854996195", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("Fecha: " & dgvVentas.SelectedRows(0).Cells("Fecha").Value, font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("Nro. Venta: " & dgvVentas.SelectedRows(0).Cells("IdVenta").Value, font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("Cantidad - Descripción", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3
        graphic.DrawString("-------------------------------------", font, New SolidBrush(Color.Black), startx, starty + offset)
        offset = offset + CInt(fontheight) + 3

        ' Detalles
        For Each row As DataGridViewRow In dgvDetalles.Rows

            Dim desc As String = row.Cells("Descripcion").Value
            desc = desc.Substring(0, Math.Min(desc.Length, 15))
            desc = desc & ". " & row.Cells("Marca").Value & " " & row.Cells("Color").Value & " " & row.Cells("Talle").Value
            Dim cant As String = row.Cells("Cantidad").Value
            Dim linea As String = cant & "u - " & desc

            graphic.DrawString(linea, font, New SolidBrush(Color.Black), New RectangleF(startx, starty + offset, 300, 50))

            offset = offset + CInt(fontheight) + 20
        Next

        offset = offset + CInt(fontheight) + 10

        graphic.DrawString("Comprobante no válido como factura", font, New SolidBrush(Color.Black), 15, starty + offset)
        offset = offset + 20
        graphic.DrawString("¡Gracias por su compra!", font, New SolidBrush(Color.Black), 63, starty + offset)
        offset = offset + CInt(fontheight) + 20
        graphic.DrawString("", font, New SolidBrush(Color.Black), 63, starty + offset)

        e.HasMorePages = False

    End Sub

End Class