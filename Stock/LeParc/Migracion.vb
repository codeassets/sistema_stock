﻿Public Class Migracion
    Private Sub Migracion_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        Dim venta As New DatosMigracion

        dgvVentas.DataSource = venta.ObtenerTodoVentasViejas

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        For Each row As DataGridViewRow In dgvVentas.Rows

            Dim venta As New DatosMigracion
            Dim idventa As New Int64

            venta.IdCliente_ = row.Cells("IdCliente").Value
            venta.Fecha_ = row.Cells("Fecha").Value
            venta.Tipo_ = row.Cells("Tipo").Value
            venta.Pago_ = row.Cells("Pago").Value
            venta.Descuento_ = row.Cells("Descuento").Value
            venta.Total_ = row.Cells("Importe").Value
            venta.IdUsuario_ = 1
            idventa = venta.InsertaVentas()

            venta.IdProducto_ = row.Cells("IdProducto").Value
            venta.Descripcion_ = row.Cells("Descripcion").Value
            venta.Marca_ = row.Cells("Marca").Value
            venta.Color_ = row.Cells("Color").Value
            venta.Talle_ = row.Cells("Talle").Value
            venta.Cantidad_ = row.Cells("Cantidad").Value
            venta.PrecioUnitario_ = row.Cells("Importe").Value / row.Cells("Cantidad").Value
            venta.Subtotal_ = row.Cells("Importe").Value
            venta.IdVentas_ = idventa
            venta.CrearDetalleVenta()

            dgvVentas2.DataSource = venta.ObtenerVentasNuevas
            dgvDetalles.DataSource = venta.ObtenerDetalles

        Next

    End Sub

End Class