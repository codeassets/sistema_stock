﻿Public Class Faltantes

    Dim datosprod As New DatosProductos

    Private Sub dgvStock_Sorted(sender As Object, e As EventArgs) Handles dgvStock.Sorted

        If dgvStock.RowCount <> 0 Then
            For Each row As DataGridViewRow In dgvStock.Rows
                row.Cells("NombreColor").Style.BackColor = Color.FromArgb(Convert.ToInt32(row.Cells("Color").Value))
            Next
        End If

    End Sub

    Private Sub bttAceptar_Click(sender As Object, e As EventArgs) Handles bttAceptar.Click

        Try
            dgvStock.DataSource = datosprod.ObtenerFaltantes(NumCant.Value)

            dgvStock.Columns("IdProducto").Visible = False
            dgvStock.Columns("Color").Visible = False

            dgvStock.Columns("Codigo").FillWeight = 10
            dgvStock.Columns("Descripcion").FillWeight = 20
            dgvStock.Columns("Marca").FillWeight = 10
            dgvStock.Columns("Cantidad").FillWeight = 10
            dgvStock.Columns("Talle").FillWeight = 10
            dgvStock.Columns("NombreColor").FillWeight = 10
            dgvStock.Columns("PrecioMinorista").FillWeight = 10
            dgvStock.Columns("PrecioMayorista").FillWeight = 10
            dgvStock.Columns("PrecioTarjeta").FillWeight = 10

            dgvStock.Columns("Codigo").HeaderText = "Código"
            dgvStock.Columns("Descripcion").HeaderText = "Descripción"
            dgvStock.Columns("NombreColor").HeaderText = "Color"
            dgvStock.Columns("PrecioMinorista").HeaderText = "$ Min."
            dgvStock.Columns("PrecioMayorista").HeaderText = "$ May."
            dgvStock.Columns("PrecioTarjeta").HeaderText = "$ Tarj."
            dgvStock.Columns("Cantidad").HeaderText = "Cant."

            If dgvStock.RowCount <> 0 Then
                For Each row As DataGridViewRow In dgvStock.Rows
                    row.Cells("NombreColor").Style.BackColor = Color.FromArgb(Convert.ToInt32(row.Cells("Color").Value))
                Next
            End If

            dgvStock.ClearSelection()
        Catch ex As Exception
            MessageBox.Show("Ocurrio un error al cargar los Productos. Detalle:" & ex.Message.ToString)
        End Try

    End Sub

    Private Sub NumCant_KeyDown(sender As Object, e As KeyEventArgs) Handles NumCant.KeyDown

        If e.KeyCode = Keys.E.Enter Then
            bttAceptar.PerformClick()
        End If

    End Sub
    Private Sub Faltantes_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class