﻿Public Class Config

    Private Sub bttMarcas_Click(sender As Object, e As EventArgs) Handles bttMarcas.Click
        PanelColor.Location = New Point(66, 123)
        PanelALL.Controls.Clear()
        Dim f As New ConfigMarcas With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub bttTalles_Click(sender As Object, e As EventArgs) Handles bttTalles.Click
        PanelColor.Location = New Point(164, 123)
        PanelALL.Controls.Clear()
        Dim f As New ConfigTalles With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        PanelColor.Location = New Point(253, 123)
        PanelALL.Controls.Clear()
        Dim f As New ConfigImpresora With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub

    Private Sub Config_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        bttMarcas.PerformClick()
    End Sub

    Private Sub bttVendedores_Click(sender As Object, e As EventArgs) Handles bttVendedores.Click
        PanelColor.Location = New Point(253, 377)
        PanelALL.Controls.Clear()
        Dim f As New ConfigVendedores With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
    End Sub
End Class