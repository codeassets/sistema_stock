﻿Imports System.Data.SqlClient

Public Class DatosClientes
    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

#Region "Get set"

    Private IdCliente As Integer
    Public Property IdCliente_() As Integer
        Get
            Return IdCliente
        End Get
        Set(ByVal value As Integer)
            IdCliente = value
        End Set
    End Property

    Private Documento As Int64
    Public Property Documento_() As Int64
        Get
            Return Documento
        End Get
        Set(ByVal value As Int64)
            Documento = value
        End Set
    End Property

    Private Nombre As String
    Public Property Nombre_() As String
        Get
            Return Nombre
        End Get
        Set(ByVal value As String)
            Nombre = value
        End Set
    End Property

    Private Apellido As String
    Public Property Apellido_() As String
        Get
            Return Apellido
        End Get
        Set(ByVal value As String)
            Apellido = value
        End Set
    End Property

    Private Direccion As String
    Public Property Direccion_() As String
        Get
            Return Direccion
        End Get
        Set(ByVal value As String)
            Direccion = value
        End Set
    End Property

    Private Telefono As String
    Public Property Telefono_() As String
        Get
            Return Telefono
        End Get
        Set(ByVal value As String)
            Telefono = value
        End Set
    End Property

    Private Email As String
    Public Property Email_() As String
        Get
            Return Email
        End Get
        Set(ByVal value As String)
            Email = value
        End Set
    End Property

    Private DirImagen As String
    Public Property DirImagen_() As String
        Get
            Return DirImagen
        End Get
        Set(ByVal value As String)
            DirImagen = value
        End Set
    End Property
#End Region

    Public Function ObtenerTodo() As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim Accion As String

        Dim Tabla As New DataTable

        Accion = "SELECT * FROM Clientes where Activo = 1"
        Dim Adaptador As New SqlDataAdapter(Accion, Conexion)
        Tabla.Clear()

        Conexion.Open()
        Adaptador.Fill(Tabla)
        Conexion.Close()

        Return Tabla

    End Function


    Public Function Obtener_PorApellido(ByVal Apellido As String) As DataTable

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Dim Accion As String

        Dim Tabla As New DataTable

        Accion = "Select * FROM Clientes WHERE Apellido Like '%" & Apellido & "%' and Activo = 1"
        Comando.Connection = Conexion
        Comando.CommandText = Accion
        Dim Adaptador As New SqlDataAdapter(Comando)
        Tabla.Clear()

        Conexion.Open()
        Adaptador.Fill(Tabla)
        Conexion.Close()

        Return Tabla

    End Function

    Public Sub InsertarCliente()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdCliente", IdCliente)
        Comando.Parameters.AddWithValue("@Documento", Documento)
        Comando.Parameters.AddWithValue("@Nombre", Nombre)
        Comando.Parameters.AddWithValue("@Apellido", Apellido)
        Comando.Parameters.AddWithValue("@Direccion", Direccion)
        Comando.Parameters.AddWithValue("@Telefono", Telefono)
        Comando.Parameters.AddWithValue("@Email", Email)
        Comando.Parameters.AddWithValue("@DirImagen", DirImagen)

        Comando.CommandText = "INSERT INTO Clientes(Documento, Nombre, Apellido, Direccion, Telefono, Email, DirImagen) " +
                               "VALUES(@Documento, @Nombre, @Apellido, @Direccion, @Telefono, @Email, @DirImagen)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub NuevoCliente()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@Documento", Documento)
        Comando.Parameters.AddWithValue("@Nombre", Nombre)
        Comando.Parameters.AddWithValue("@Apellido", Apellido)
        Comando.Parameters.AddWithValue("@Direccion", Direccion)
        Comando.Parameters.AddWithValue("@Telefono", Telefono)
        Comando.Parameters.AddWithValue("@Email", Email)

        Comando.CommandText = "INSERT INTO Clientes(Documento, Nombre, Apellido, Direccion, Telefono, Email) " +
                               "VALUES(@Documento, @Nombre, @Apellido, @Direccion, @Telefono, @Email)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ModificarCliente()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdCliente", IdCliente)
        Comando.Parameters.AddWithValue("@Documento", Documento)
        Comando.Parameters.AddWithValue("@Nombre", Nombre)
        Comando.Parameters.AddWithValue("@Apellido", Apellido)
        Comando.Parameters.AddWithValue("@Direccion", Direccion)
        Comando.Parameters.AddWithValue("@Telefono", Telefono)
        Comando.Parameters.AddWithValue("@Email", Email)
        Comando.Parameters.AddWithValue("@DirImagen", DirImagen)

        Comando.CommandText = "UPDATE Clientes Set Documento=@Documento, Nombre = @Nombre," +
        "Apellido = @Apellido, Direccion = @Direccion, Telefono = @Telefono, Email = @Email, DirImagen = @DirImagen" +
        " WHERE IdCliente = @IdCliente"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Function buscar(dt As DataTable, text As String) As Object
        Using adaptador As New SqlDataAdapter("Select * " +
                                              "from Clientes " +
                                              "where Activo = 1 and Documento  Like '" & "%" + text + "%" & "' or Nombre  LIKE '" & "%" + text + "%" & "' or Apellido  LIKE '" & "%" + text + "%" & "' or Direccion  LIKE '" & "%" + text + "%" & "' or Telefono  LIKE '" & "%" + text + "%" & "'  ORDER BY Apellido", Cadena)
            adaptador.Fill(dt)
        End Using
        Return dt
    End Function

    Public Function ObtenerConDocumento() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("select * from Clientes where Documento= @Documento", Cadena)
            adaptador.SelectCommand.Parameters.AddWithValue("@Documento", Documento)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function

    Public Sub EliminarCliente()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdCliente", IdCliente)
        Comando.CommandText = "DELETE from Clientes WHERE IdCliente = @IdCliente"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub
End Class
