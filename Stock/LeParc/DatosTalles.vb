﻿Imports System.Data.SqlClient

Public Class DatosTalles

    Private ReadOnly Property Cadena() As String
        Get
            Return My.Settings.Conexion
        End Get
    End Property

#Region "GetSet"

    Private IdTalle As Int64
    Public Property IdTalle_() As Int64
        Get
            Return IdTalle
        End Get
        Set(ByVal value As Int64)
            IdTalle = value
        End Set
    End Property

    Private Nombre As String
    Public Property Nombre_() As String
        Get
            Return Nombre
        End Get
        Set(ByVal value As String)
            Nombre = value
        End Set
    End Property

#End Region


    Public Sub InsertarTalle()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand
        Comando.Parameters.AddWithValue("@Nombre", Nombre)
        Comando.CommandText = "INSERT INTO Talles(Nombre) " +
                               "VALUES(@Nombre)"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub ModificarTalle()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand


        Comando.Parameters.AddWithValue("@IdTalle", IdTalle)
        Comando.Parameters.AddWithValue("@Nombre", Nombre)

        Comando.CommandText = "UPDATE Talles SET Nombre = @Nombre WHERE IdTalle = @IdTalle"

        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Sub EliminarTalle()

        Dim Conexion As New SqlConnection(Cadena)
        Dim Comando As New SqlCommand

        Comando.Parameters.AddWithValue("@IdTalle", IdTalle)
        Comando.CommandText = "UPDATE Talles SET Activo = 0 WHERE IdTalle = @IdTalle"
        Comando.Connection = Conexion
        Conexion.Open()
        Comando.ExecuteNonQuery()
        Conexion.Close()

    End Sub

    Public Function ObtenerTodo() As DataTable

        Dim dt As New DataTable
        Using adaptador As New SqlDataAdapter("SELECT * from Talles WHERE Activo = 1 ORDER BY Nombre ASC", Cadena)
            adaptador.Fill(dt)
        End Using
        Return dt

    End Function

End Class
