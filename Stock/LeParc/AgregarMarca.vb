﻿Imports System.Text.RegularExpressions

Public Class AgregarMarca

    Private Sub bttAgregar_Click(sender As Object, e As EventArgs) Handles bttAgregar.Click
        Try
            Dim marca As New DatosProductos
            Dim cleanString As String = Regex.Replace(txtMarca.Text, "[^A-Za-z0-9\ ]", "")
            marca.Marca_ = cleanString
            marca.InsertarMarca()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show("Ocurrio un Problema al cargar la Marca. Detalle: " & ex.Message.ToString)
        End Try
    End Sub
End Class