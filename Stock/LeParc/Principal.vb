﻿Public Class FormPrincipal

    Dim datosventas As New DatosVentas
    Dim datosind As New DatosProductos

    Dim idprod As Integer
    Dim total As Double
    Dim totalmay As Double
    Dim totaltarj As Double
    Dim cantidad As Integer
    Dim Stock As Integer

    Public idcliente As Integer
    Public idprod_buscar As Integer = 0

    ' para login AFIP
    Public l As LoginAFIP
    'url homologacion
    Private url As String = "https://wsaahomo.afip.gov.ar/ws/services/LoginCms"

    'url produccion
    'Private url As String = "https://wsaa.afip.gov.ar/ws/services/LoginCms?wsdl"

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles BttClientes.Click

        PanelColor.Location = New Point(3, 127)
        BttNuevaVenta.Image = My.Resources.nuevaventa
        BttProductos.Image = My.Resources.prod
        BttClientes.Image = My.Resources.clientes1
        BttVentas.Image = My.Resources.ventas
        bttInforme.Image = My.Resources.informes1
        BttAbout.Image = My.Resources.cuentas
        bttFacturas.Image = My.Resources.facturas
        PanelALL.Controls.Clear()
        Dim f As New Clientes With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = Windows.Forms.FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
        PanelALL.Visible = True

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles BttProductos.Click

        PanelColor.Location = New Point(3, 71)
        BttNuevaVenta.Image = My.Resources.nuevaventa
        BttProductos.Image = My.Resources.prod1
        BttClientes.Image = My.Resources.clientes
        BttVentas.Image = My.Resources.ventas
        bttInforme.Image = My.Resources.informes1
        BttAbout.Image = My.Resources.cuentas
        bttFacturas.Image = My.Resources.facturas
        PanelALL.Controls.Clear()
        Dim f As New Productos With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = Windows.Forms.FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
        PanelALL.Visible = True
        f.txtBuscar.Focus()

    End Sub

    Private Sub Button3_Click_1(sender As Object, e As EventArgs) Handles BttVentas.Click

        PanelColor.Location = New Point(3, 183)
        BttNuevaVenta.Image = My.Resources.nuevaventa
        BttProductos.Image = My.Resources.prod
        BttClientes.Image = My.Resources.clientes
        BttVentas.Image = My.Resources.ventas1
        bttInforme.Image = My.Resources.informes1
        BttAbout.Image = My.Resources.cuentas
        bttFacturas.Image = My.Resources.facturas
        PanelALL.Controls.Clear()
        Dim f As New Ventas With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = Windows.Forms.FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
        PanelALL.Visible = True

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles bttInforme.Click

        PanelColor.Location = New Point(3, 239)
        BttNuevaVenta.Image = My.Resources.nuevaventa
        BttProductos.Image = My.Resources.prod
        BttClientes.Image = My.Resources.clientes
        BttVentas.Image = My.Resources.ventas
        bttInforme.Image = My.Resources.informes11
        BttAbout.Image = My.Resources.cuentas
        bttFacturas.Image = My.Resources.facturas
        PanelALL.Controls.Clear()
        Dim f As New Informes With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = Windows.Forms.FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
        PanelALL.Visible = True

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles BttAbout.Click

        PanelColor.Location = New Point(3, 295)
        BttNuevaVenta.Image = My.Resources.nuevaventa
        BttProductos.Image = My.Resources.prod
        BttClientes.Image = My.Resources.clientes
        BttVentas.Image = My.Resources.ventas
        bttInforme.Image = My.Resources.informes1
        BttAbout.Image = My.Resources.cuentas1
        bttFacturas.Image = My.Resources.facturas
        PanelALL.Controls.Clear()
        Dim f As New CuentasCorrientes With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = Windows.Forms.FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
        PanelALL.Visible = True

    End Sub


    Private Sub BttNuevaVenta_Click(sender As Object, e As EventArgs) Handles BttNuevaVenta.Click

        PanelColor.Location = New Point(3, 15)
        BttNuevaVenta.Image = My.Resources.nuevaventa1
        BttProductos.Image = My.Resources.prod
        BttClientes.Image = My.Resources.clientes
        BttVentas.Image = My.Resources.ventas
        bttInforme.Image = My.Resources.informes1
        BttAbout.Image = My.Resources.cuentas
        bttFacturas.Image = My.Resources.facturas
        PanelALL.Controls.Clear()
        Dim f As New NuevaVenta With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = Windows.Forms.FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)

        f.Show()
        PanelALL.Visible = True
        f.txtCodVenta.Focus()

    End Sub


    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnMantenimiento.Click
        Dim f As New pruebaPRODUCTOS
        f.Show()
    End Sub

    Private Sub FormPrincipal_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If (e.KeyCode And Not Keys.Modifiers) = Keys.L AndAlso e.Modifiers = Keys.Control Then

            btnMantenimiento.Visible = True
        End If
    End Sub

    Private Sub FormPrincipal_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

        Net.ServicePointManager.SecurityProtocol = Net.SecurityProtocolType.Tls12
        ' login AFIP
        l = New LoginAFIP(My.Settings.def_serv, My.Settings.def_url, My.Settings.def_cert, My.Settings.def_pass)
        l.hacerLogin()

        BttNuevaVenta.PerformClick()

    End Sub

    Private Sub bttSalir_Click(sender As Object, e As EventArgs) Handles bttSalir.Click
        Me.Close()
    End Sub

    Private Sub bttConfig_Click(sender As Object, e As EventArgs) Handles bttConfig.Click
        Dim f As New Config()
        f.ShowDialog()
    End Sub

    Private Sub bttAyuda_Click(sender As Object, e As EventArgs) Handles bttAyuda.Click
        Dim f As New Ayuda
        f.ShowDialog()
    End Sub

    Private Sub bttFacturas_Click(sender As Object, e As EventArgs) Handles bttFacturas.Click

        PanelColor.Location = New Point(3, 351)
        BttNuevaVenta.Image = My.Resources.nuevaventa
        BttProductos.Image = My.Resources.prod
        BttClientes.Image = My.Resources.clientes
        BttVentas.Image = My.Resources.ventas
        bttInforme.Image = My.Resources.informes1
        BttAbout.Image = My.Resources.cuentas
        bttFacturas.Image = My.Resources.facturas1
        PanelALL.Controls.Clear()
        Dim f As New Facturas With {.TopLevel = False, .AutoSize = False, .FormBorderStyle = Windows.Forms.FormBorderStyle.None}
        f.Dock = DockStyle.Fill
        Me.PanelALL.Controls.Add(f)
        f.Show()
        PanelALL.Visible = True

    End Sub

    Private Sub bttCerrarSesion_Click(sender As Object, e As EventArgs) 
        Dim f As New Migracion
        f.ShowDialog()
    End Sub

End Class