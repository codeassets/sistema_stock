﻿Public Class BuscarProducto

    Dim opcion As Integer
    Dim color As Color
    Dim idprod As Integer



    Private Sub BuscarProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            CB_Buscar.SelectedIndex = 4
            'Dim indu As New DatosProductos
            'dgvStock.DataSource = indu.ObtenerTodo()
            'dgvStock.Columns("IdProducto").Visible = False
            'dgvStock.Columns("Color").Visible = False
            'dgvStock.Columns("Activo").Visible = False
            'If dgvStock.RowCount <> 0 Then
            '    For Each row As DataGridViewRow In dgvStock.Rows
            '        row.Cells("NombreColor").Style.BackColor = Color.FromArgb(Convert.ToInt32(row.Cells("Color").Value))
            '    Next
            'End If
            'dgvStock.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            'dgvStock.Columns("Marca").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            'dgvStock.Columns("NombreColor").HeaderText = "Color"
            'dgvStock.Columns("PrecioMinorista").HeaderText = "$ Minorista"
            'dgvStock.Columns("PrecioMayorista").HeaderText = "$ Mayorista"
            'dgvStock.Columns("PrecioTarjeta").HeaderText = "$ Tarjeta"
            'dgvStock.Columns("Cantidad").HeaderText = "Cant."
            'dgvStock.ClearSelection()
            'CB_Buscar.SelectedIndex = 4
            'txtBuscar.Focus()
        Catch ex As Exception
            MessageBox.Show("Ocurrio un error al cargar los Productos. Detalle:" & ex.Message.ToString)
        End Try
    End Sub

    Private Sub bttSeleccionar_Click(sender As Object, e As EventArgs) Handles bttSeleccionar.Click
        Try
            If dgvStock.SelectedRows.Count <> 0 Then

                Me.DialogResult = DialogResult.OK
                'FormPrincipal.idprod_buscar = dgvStock.CurrentRow.Cells("IdProducto").Value
                'FormPrincipal.CB_Precio.SelectedIndex = 0
                'FormPrincipal.Cargar_Datos()

                Me.Close()
            Else
            End If
        Catch ex As Exception
            MessageBox.Show("Ocurrio un Problema al seleccionar el Producto. Detalle: " & ex.Message.ToString)
        End Try
    End Sub

    Private Sub BuscarProducto_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        dgvStock.ClearSelection()
    End Sub

    Private Sub dgvStock_Sorted(sender As Object, e As EventArgs) Handles dgvStock.Sorted
        If dgvStock.RowCount <> 0 Then
            For Each row As DataGridViewRow In dgvStock.Rows
                row.Cells("NombreColor").Style.BackColor = Color.FromArgb(Convert.ToInt32(row.Cells("Color").Value))
            Next
        End If
    End Sub

    Private Sub CB_Buscar_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles CB_Buscar.SelectionChangeCommitted
        'Dim indu As New DatosProductos

        'Dim dt As New DataTable
        'Dim Bandera As New Integer

        'If CB_Buscar.SelectedIndex = 0 Then 'codigo

        '    Bandera = 0

        'ElseIf CB_Buscar.SelectedIndex = 1 Then 'talle

        '    Bandera = 1

        'ElseIf CB_Buscar.SelectedIndex = 2 Then 'marca

        '    Bandera = 2
        'ElseIf CB_Buscar.SelectedIndex = 3 Then 'color

        '    Bandera = 3
        'ElseIf CB_Buscar.SelectedIndex = 4 Then 'descripcion

        '    Bandera = 4
        'ElseIf CB_Buscar.SelectedIndex = 5 Then 'todos


        '    Bandera = 5
        'End If

        'If Trim(txtBuscar.Text) = "" Then
        '    indu.Codigo_ = ""
        '    indu.Marca_ = ""
        '    indu.colorname_ = ""
        '    indu.Descripcion_ = ""
        '    indu.Talle_ = ""
        '    dgvStock.DataSource = indu.TraerOrdenado(dt, Bandera)
        'Else
        '    indu.Codigo_ = txtBuscar.Text
        '    indu.Marca_ = txtBuscar.Text
        '    indu.colorname_ = txtBuscar.Text
        '    indu.Descripcion_ = txtBuscar.Text
        '    indu.Talle_ = txtBuscar.Text
        '    dgvStock.DataSource = indu.Buscar(dt, Bandera)
        'End If




        'dgvStock.Columns("IdProducto").Visible = False
        'dgvStock.Columns("Color").Visible = False
        'If dgvStock.RowCount <> 0 Then
        '    For Each row As DataGridViewRow In dgvStock.Rows
        '        row.Cells("NombreColor").Style.BackColor = Color.FromArgb(Convert.ToInt32(row.Cells("Color").Value))
        '    Next
        'End If
        'dgvStock.ClearSelection()
    End Sub

    Private Sub txtBuscar_KeyDown(sender As Object, e As KeyEventArgs)

    End Sub

    Private Sub TextBox1_KeyDown(sender As Object, e As KeyEventArgs) Handles txtBuscar.KeyDown
        Try


            If e.KeyCode = Keys.Enter Then
                Dim indu As New DatosProductos
                If (txtBuscar.Text <> "") Then
                    Dim dt As New DataTable
                    Dim Bandera As New Integer

                    'If CB_Buscar.SelectedIndex = 0 Then 'codigo
                    '    indu.Codigo_ = txtBuscar.Text
                    '    Bandera = 0

                    If CB_Buscar.SelectedIndex = 0 Then 'talle
                        indu.Talle_ = txtBuscar.Text
                        Bandera = 1

                    ElseIf CB_Buscar.SelectedIndex = 1 Then 'marca
                        indu.Marca_ = txtBuscar.Text
                        Bandera = 2
                    ElseIf CB_Buscar.SelectedIndex = 2 Then 'color
                        indu.colorname_ = txtBuscar.Text
                        Bandera = 3
                    ElseIf CB_Buscar.SelectedIndex = 3 Then 'descripcion
                        indu.Descripcion_ = txtBuscar.Text
                        Bandera = 4
                    ElseIf CB_Buscar.SelectedIndex = 4 Then 'todos
                        If (txtBuscar.Text.Contains(" ")) Then
                            indu.Descripcion_ = txtBuscar.Text.Split(" ")(0).Replace(" ", "")
                            indu.Marca_ = txtBuscar.Text.Split(" ")(1).Replace(" ", "")
                        Else
                            indu.Descripcion_ = txtBuscar.Text
                        End If

                        Bandera = 5
                    End If


                    dgvStock.DataSource = indu.Buscar(dt, Bandera)
                Else
                    dgvStock.DataSource = indu.ObtenerTodo()
                End If

                dgvStock.Columns("IdProducto").Visible = False
                dgvStock.Columns("Codigo").Visible = False
                dgvStock.Columns("Color").Visible = False
                dgvStock.Columns("Activo").Visible = False
                dgvStock.Columns("IdUsuario_A").Visible = False
                dgvStock.Columns("IdUsuario_M").Visible = False
                dgvStock.Columns("Fecha_A").Visible = False
                dgvStock.Columns("Fecha_M").Visible = False


                If dgvStock.RowCount <> 0 Then
                    For Each row As DataGridViewRow In dgvStock.Rows
                        row.Cells("NombreColor").Style.BackColor = Color.FromArgb(Convert.ToInt32(row.Cells("Color").Value))
                    Next
                End If
                dgvStock.Columns("Descripcion").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                dgvStock.ClearSelection()
            End If
        Catch ex As Exception
            MessageBox.Show("Ocurrio un error al buscar un producto" & ex.Message.ToString)
        End Try
    End Sub

    Private Sub txtBuscar_Click(sender As Object, e As EventArgs) Handles txtBuscar.Click
        If txtBuscar.Text = "Presione ENTER para buscar" Then
            txtBuscar.Text = ""
        Else
            txtBuscar.SelectAll()
        End If
    End Sub

    Private Sub txtBuscar_Leave(sender As Object, e As EventArgs) Handles txtBuscar.Leave
        If Trim(txtBuscar.Text) = "" Then
            txtBuscar.Text = "Presione ENTER para buscar"
        End If
    End Sub
End Class